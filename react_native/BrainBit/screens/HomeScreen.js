import React from 'react';
import { View, Text , StyleSheet, StatusBar, SafeAreaView} from 'react-native';
import FlatButton from '../UIComponents/FlatButton';
import Separator from '../UIComponents/Separator';
import {DeviceState,EventTypes} from '../DeviceManager';

class HomeScreen extends React.Component 
{ 
  constructor(props) 
  {
    super(props);
    this.state = {disabledButton: true}
    this.deviceManager = props.route.params.deviceManager;
    this.deviceManager.deviceManagerEmitter.addListener(EventTypes.DeviceStateChanged, (deviceState) => {
     this.setState({disabledButton: (deviceState == DeviceState.DISCONNECTED)})
    });
  }
  componentDidMount(){
    
  }
  render(){ 
    return <SafeAreaView style={styles.container}>
              <FlatButton
                disabled={false}
                text="Device search"
                onPress={() => this.props.navigation.navigate('DeviceSearch')}
              />
              <Separator/>
              <FlatButton
                disabled={this.state.disabledButton}
                text="Signal"
                onPress={() => this.props.navigation.navigate('Signal')}
              />
              <Separator/>
              <FlatButton
                disabled={this.state.disabledButton}
                text="Resistance"
                onPress={() => this.props.navigation.navigate('Resistance')}
              />
            </SafeAreaView>
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    marginHorizontal: 70,
    marginTop: StatusBar.currentHeight || 10,
  },
  button:{
    flex: .1,
  }
});
export default HomeScreen;