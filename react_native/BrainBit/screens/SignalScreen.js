import React from "react";
import { StyleSheet, View} from "react-native";
import { START_SIGNAL, STOP_SIGNAL } from "react-native-neurosdk";
import {SignalLabel} from '../UIComponents/SignalLabel'; 

export default class SignalScreen extends React.Component {
  constructor(props){
    super(props);
    this.values = [];
    this.state = {text1: 0,text2: 0, text3: 0,text4: 0};
    console.log("SignalScreen constructor")
    this.deviceManager = props.route.params.deviceManager;
    this.deviceManager.device.executeCommand(START_SIGNAL)

    this.interval = setInterval(()=>{
      this.deviceManager.signalChannels[0].totalLength().then((totalLength)=>{
          this.setState({text1: this.deviceManager.signalChannels[0].readDataSync(totalLength.totalLength-1,1)[0]})
      });
      this.deviceManager.signalChannels[1].totalLength().then((totalLength)=>{
        this.setState({text2: this.deviceManager.signalChannels[1].readDataSync(totalLength.totalLength-1,1)[0]})
      });
      this.deviceManager.signalChannels[2].totalLength().then((totalLength)=>{
      this.setState({text3: this.deviceManager.signalChannels[2].readDataSync(totalLength.totalLength-1,1)[0]})
      });
      this.deviceManager.signalChannels[3].totalLength().then((totalLength)=>{
        this.setState({text4: this.deviceManager.signalChannels[3].readDataSync(totalLength.totalLength-1,1)[0]})
      })
    }, 1000);
  }
  componentWillUnmount()
  {
    this.deviceManager.signalChannels.forEach(signal => {
      signal.removeLengthChanged();
    });
    this.deviceManager.device.executeCommand(STOP_SIGNAL);
    clearInterval(this.interval);
  }
  render() {
    return (
      <View style={styles.container}>
        <SignalLabel channelName={this.deviceManager.signalChannels[0].channelInfo.name} value={this.state.text1}></SignalLabel>
        <SignalLabel channelName={this.deviceManager.signalChannels[1].channelInfo.name} value={this.state.text2}></SignalLabel>
        <SignalLabel channelName={this.deviceManager.signalChannels[2].channelInfo.name} value={this.state.text3}></SignalLabel>
        <SignalLabel channelName={this.deviceManager.signalChannels[3].channelInfo.name} value={this.state.text4}></SignalLabel>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#f5fcff"
  },
  text: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 16,
    textAlign: 'center',
  }
});