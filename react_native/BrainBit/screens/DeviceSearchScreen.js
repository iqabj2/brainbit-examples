import React, { useState } from 'react';
import { View, Text, FlatList, StyleSheet ,SafeAreaView,StatusBar} from 'react-native';
import FlatButton from '../UIComponents/FlatButton.js';
import DeviceListItem from '../UIComponents/DeviceListItem'

class DeviceSearchScreen extends React.Component
{ 
  constructor(props) 
  {
    super(props);
    this.deviceManager = props.route.params.deviceManager;
    this.state = {searchButtonText: "Start search", deviceList: null, selectedId: null};
    this.renderItem = this.renderItem.bind(this);
    this.searchButtonClickHandler = this.searchButtonClickHandler.bind(this);
  }
  componentDidMount(){
    
  }
  componentWillUnmount()
  {
    this.deviceManager.stopScan();
  }
  searchButtonClickHandler()
  {
    if(this.state.searchButtonText == "Start search"){
      this.deviceManager.startScan((deviceList => {
        this.setState({deviceList: deviceList});
      }));
      this.setState({searchButtonText: "Stop search"});
    }
    else
    {
      this.deviceManager.stopScan();
      this.setState({searchButtonText: "Start search"});
    }
  }
 
  renderItem({item})
  {
    //const backgroundColor = item.address === this.state.selectedId ? "#6e3b6e" : "#f9c2ff";
    //const color = item.address === this.state.selectedId ? 'white' : 'black';
    const backgroundColor = "#f9c2ff";
    const color = 'white';
    return (
      <DeviceListItem
        item={item}
        onPress={() => {
          console.log(item);
          this.deviceManager.connect(item).then(()=>{
            this.props.navigation.goBack();
          }).catch((e)=>{
            console.log(e);
          });
        }}
        backgroundColor={{ backgroundColor }}
        textColor={{ color }}
      />
    );
  };
  render(){
    return(
    <SafeAreaView style={styles.container}>
        <View style={styles.button}>
          <FlatButton
            key="searchButton"
            text={this.state.searchButtonText}
            onPress={() => {
              this.searchButtonClickHandler();
            }}
          />
        </View>
        <FlatList 
          style={styles.list}
          data={this.state.deviceList}
          renderItem={(item) => this.renderItem(item, this)}
          keyExtractor={(item) => item.address}
          extraData={this.state.selectedId}
        />
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    marginTop: StatusBar.currentHeight || 10,
  },
  button:{
    flex: .1,
    marginHorizontal: 20
  },
  list:{
    flex: 0.5
  },
  item: {
    borderRadius: 8,
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  }
});
export default DeviceSearchScreen;