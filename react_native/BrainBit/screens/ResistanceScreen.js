import React from "react";
import { StyleSheet, View, Dimensions, Text} from "react-native";
import { START_RESIST, STOP_RESIST} from "react-native-neurosdk";
import { LineChart } from "react-native-chart-kit";
import Separator from '../UIComponents/Separator.js'; 
import {SignalLabel} from '../UIComponents/SignalLabel'; 

export default class ResistanceScreen extends React.Component {
  constructor(props){
    super(props);
    this.values = [];
    this.state = {data: [100,200,300],text1: 0,text2: 0, text3: 0,text4: 0};
    console.log("ResistanceScreen constructor")
    this.deviceManager = props.route.params.deviceManager;
    this.deviceManager.device.executeCommand(START_RESIST)
    
    this.interval = setInterval(()=>{
      this.deviceManager.resistanceChannels[0].totalLength().then((totalLength)=>{
          this.setState({text1: this.deviceManager.resistanceChannels[0].readDataSync(totalLength.totalLength-1,1)[0]})
      });
      this.deviceManager.resistanceChannels[1].totalLength().then((totalLength)=>{
        this.setState({text2: this.deviceManager.resistanceChannels[1].readDataSync(totalLength.totalLength-1,1)[0]})
      });
      this.deviceManager.resistanceChannels[2].totalLength().then((totalLength)=>{
      this.setState({text3: this.deviceManager.resistanceChannels[2].readDataSync(totalLength.totalLength-1,1)[0]})
      });
      this.deviceManager.resistanceChannels[3].totalLength().then((totalLength)=>{
        this.setState({text4: this.deviceManager.resistanceChannels[3].readDataSync(totalLength.totalLength-1,1)[0]})
      })
    }, 1000);
  }
  componentWillUnmount()
  {
    this.deviceManager.resistanceChannels.forEach(resistance => {
      resistance.removeLengthChanged();
    });
    this.deviceManager.resistanceChannels.forEach(resistance => {
      resistance.removeLengthChanged();
    });
    this.deviceManager.device.executeCommand(STOP_RESIST)
    clearInterval(this.interval);
  }
  render() {
    return (
      <View style={styles.container}>
        <SignalLabel channelName={this.deviceManager.resistanceChannels[0].channelInfo.name} value={this.state.text1}></SignalLabel>
        <SignalLabel channelName={this.deviceManager.resistanceChannels[1].channelInfo.name} value={this.state.text2}></SignalLabel>
        <SignalLabel channelName={this.deviceManager.resistanceChannels[2].channelInfo.name} value={this.state.text3}></SignalLabel>
        <SignalLabel channelName={this.deviceManager.resistanceChannels[3].channelInfo.name} value={this.state.text4}></SignalLabel>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#f5fcff"
  },
  text: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 16,
    textAlign: 'center',
  }
});