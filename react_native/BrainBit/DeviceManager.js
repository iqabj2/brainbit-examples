import * as React from 'react';

import { RNNeurosdk, 
  DEVICE_LIST_CHANGED_EVENT_NAME,
  DEVICE_PARAMETER_CHANGED_EVENT_NAME,
  SIGNAL_CHANNEL_LENGTH_CHANGED_EVENT_NAME,
  RESISTANCE_CHANNEL_LENGTH_CHANGED_EVENT_NAME,
  BATTERY_CHANNEL_LENGTH_CHANGED_EVENT_NAME,
  BATTERY_CHANNEL,
  SIGNAL_CHANNEL,
  RESISTANCE_CHANNEL } from 'react-native-neurosdk'
import EventEmitter from 'react-native/Libraries/vendor/emitter/EventEmitter';

import { NativeEventEmitter } from 'react-native';

class DeviceEnumerator
{
    constructor()
    {
         console.log("DeviceEnumerator constructor");
    }
    
    create()
    {
        console.log("DeviceEnumerator created");
        return RNNeurosdk.CreateDeviceEnumerator();   
    }
    close()
    {
        console.log("DeviceEnumerator deleted");
        return RNNeurosdk.DeleteDeviceEnumerator();
    }

    devices() { 
        return RNNeurosdk.Devices();
    }
    
    addDeviceListChanged(callback)
    {
        var deviceListChangedEmitter = new NativeEventEmitter(RNNeurosdk);
        this.listenerDeviceListChanged = deviceListChangedEmitter.addListener(
            DEVICE_LIST_CHANGED_EVENT_NAME,
            () => callback()
        );
    }
    removeDeviceListChanged(){
        if(this.listenerDeviceListChanged != undefined){  
            this.listenerDeviceListChanged.remove()  
        }   
    }

    createDevice(deviceInfo)
    {
        console.log(RNNeurosdk.CreateDevice(deviceInfo));
        return new Device(deviceInfo);  
    }
    bondTo(deviceInfo){
        return RNNeurosdk.BondTo(deviceInfo.address);
    }
}



class Device
{
    constructor(deviceInfo) {
        console.log("Device constructor");
        this.deviceInfo = deviceInfo;
     }

    connect(){
        return RNNeurosdk.Connect(this.deviceInfo);
    }
    executeCommand(command){
        console.log("executeCommand: " + command);
        return RNNeurosdk.Execute(this.deviceInfo, command);    
    }
    
    addParameterChanged(callback){
        var deviceListChangedEmitter = new NativeEventEmitter(RNNeurosdk);
        this.listenerListChanged = deviceListChangedEmitter.addListener(
        DEVICE_PARAMETER_CHANGED_EVENT_NAME,
        (params) => {
            if(params.address === this.deviceInfo.address){
                callback(params.state)
            }
        }
      );
    }
    removeParameterChanged(){
        if(this.listenerListChanged != undefined)
            this.listenerListChanged.remove();
    }

    channels(){
        return RNNeurosdk.Channels(this.deviceInfo);
    }

    commands(){
        return RNNeurosdk.Commands(this.deviceInfo);
    }

    parameters(){
        return RNNeurosdk.Parameters(this.deviceInfo);
    }

    disconnect(){
        return RNNeurosdk.Disconnect(this.deviceInfo);
    }

    close(){
        return RNNeurosdk.Close(this.deviceInfo);
    }

}
class Channel
{
    constructor(deviceInfo, channelInfo){
        console.log(channelInfo.type + " channel constructor");
        this.channelInfo = channelInfo;
        this.deviceInfo = deviceInfo;
        this.create = this.create.bind(this);
    }
    create(){
        console.log(this.channelInfo.type + " channel " + this.channelInfo.name +" create");       
        return RNNeurosdk.CreateChannel(this.deviceInfo, this.channelInfo);
    }
    removeLengthChanged(){
        if(this.listenerLengthChanged != undefined)
            this.listenerLengthChanged.remove()
    }
    totalLength() {
        return RNNeurosdk.TotalLengthChannel(this.deviceInfo,this.channelInfo);
    }
    readData(offset, length) {
        return RNNeurosdk.ReadDataChannel(this.deviceInfo,this.channelInfo, offset, length);
    }
    readDataSync(offset, length) {
        return RNNeurosdk.ReadDataSyncChannel(this.deviceInfo,this.channelInfo, offset, length);
    }

    close(){
        return RNNeurosdk.CloseChannel(this.deviceInfo,this.channelInfo);
    }
}
class SignalChannel extends Channel
{ 
    addLengthChanged(callback){
        console.log("addLengthChanged " + this.channelInfo.name);
        var lengthChangedEmitter = new NativeEventEmitter(RNNeurosdk);
        this.listenerLengthChanged = lengthChangedEmitter.addListener(
        SIGNAL_CHANNEL_LENGTH_CHANGED_EVENT_NAME,
        (params) => {
            if(params.name === this.channelInfo.name){
                    callback(params.length)
                }
        }
        );
    }
}
class ResistanceChannel extends Channel
{
    addLengthChanged(callback){
        var lengthChangedEmitter = new NativeEventEmitter(RNNeurosdk);
        this.listenerLengthChanged = lengthChangedEmitter.addListener(
        RESISTANCE_CHANNEL_LENGTH_CHANGED_EVENT_NAME,
        (params) => {
            if(params.name === this.channelInfo.name){
                    callback(params.length)
                }
        }
        );
    }
}
class BatteryChannel extends Channel
{ 
    constructor(device){
        super(device,{name:BATTERY_CHANNEL, type:BATTERY_CHANNEL,index: 0})
    }
    addLengthChanged(callback){
        var lengthChangedEmitter = new NativeEventEmitter(RNNeurosdk);
        this.listenerLengthChanged = lengthChangedEmitter.addListener(
        BATTERY_CHANNEL_LENGTH_CHANGED_EVENT_NAME,
        (params) => {
            if(params.type === BATTERY_CHANNEL){
               callback(params.length)
            }
        }
      );
    }
}

const EventTypes = Object.freeze({
    DeviceStateChanged: "deviceStateChanged",
    DevicePowerProcentChanged: "powerProcentChanged"
});
const DeviceState = Object.freeze({
  CONNECTED: "Connected",
  DISCONNECTED: "Disconnected"
});
class DeviceManager
{
  constructor() {
    console.log("DeviceManager constructor");

    this.state = {deviceState: DeviceState.DISCONNECTED, powerProcent: "-"};

    this.deviceManagerEmitter = new EventEmitter();
    this.deviceEnumerator = new DeviceEnumerator();

    this.device = undefined; 
    this.batteryChannel = undefined;
    this.resistanceChannels = [];
    this.signalChannels = [];

    this.connect = this.connect.bind(this);
    this.disconnect = this.disconnect.bind(this);
    this.startScan = this.startScan.bind(this);
    this.stopScan = this.stopScan.bind(this);
  }
  connect(deviceInfo)
  {
    return new Promise(async (resolve, reject) => {
      this.device = this.deviceEnumerator.createDevice(deviceInfo);
      if(deviceInfo.name === "BrainBit Black"){
        await this.deviceEnumerator.BondTo(deviceInfo).then((state)=>{
          console.log("bondTo: " + state);
        }).catch((error)=>{
          reject(error);
        }); 
      }
      await this.device.connect().then(async ()=>{
        this.batteryChannel = new BatteryChannel(this.device.deviceInfo);
        this.batteryChannel.create();
        this.device.channels().then(channels=>{
          channels.forEach(channelInfo => {
            switch(channelInfo.type){
              case SIGNAL_CHANNEL:
                var signalChannel = new SignalChannel(this.device.deviceInfo,channelInfo);
                signalChannel.create().then(state => {
                  this.signalChannels.push(signalChannel);
                }).catch((error)=>{
                  reject(error);
                });
                break;
              case RESISTANCE_CHANNEL:
                var resistanceChannel = new ResistanceChannel(this.device.deviceInfo,channelInfo);
                resistanceChannel.create().then(state=>{
                    this.resistanceChannels.push(resistanceChannel);
                }).catch((error)=>{
                  reject(error);
                });
                break;
            }
          });
        }).catch((error)=>{
          this.disconnect();
          reject(error);
        })
        this.device.addParameterChanged((state) => {
          this.deviceManagerEmitter.emit(EventTypes.DeviceStateChanged,state);
        });
        this.batteryChannel.addLengthChanged(length =>{
          this.batteryChannel.totalLength().then(totalLength =>{
            this.batteryChannel.readData(totalLength.totalLength - 1,1).then((data)=>{
              this.deviceManagerEmitter.emit(EventTypes.DevicePowerProcentChanged,data[0]);
            });
          }); 
        });
        resolve(DeviceState.CONNECTED);      
      }).catch((error)=>{
        this.disconnect();
        reject(error);
      });
    }); 
  }
  disconnect()
  {
    return new Promise(async (resolve, reject) => {
      if(this.BatteryChannel != undefined){
        await this.batteryChannel.close().then(()=>{
          this.batteryChannel.removeLengthChanged();
        }).catch((error)=>{
          reject(error);
        });
        this.batteryChannel = undefined;
      }
      if(this.signalChannels != undefined){
        await this.signalChannels.forEach(signal => {
          signal.close().then(()=>{
            signal.removeLengthChanged();
          }).catch((error)=>{
            reject(error);
          });
        }); 
        this.signalChannels = [];
      }
      if(this.resistanceChannels != undefined){
        await this.resistanceChannels.forEach(resistance => {
          resistance.close().then(()=>{ 
            resistance.removeLengthChanged();
          }).catch((error)=>{
            reject(error);
          });
        });
        this.resistanceChannels = [];
      }
      if(this.device != undefined){
        await this.device.disconnect().then(r =>{ 
          this.device.removeParameterChanged();
        }).catch((error)=>{
          reject(error);
        });
        this.device = undefined; 
      }
      this.deviceManagerEmitter.emit(EventTypes.DeviceStateChanged, DeviceState.DISCONNECTED);
      resolve(DeviceState.DISCONNECTED);
    });
  }
  startScan(callback)
  {
    if(this.device != undefined){
      this.disconnect().catch((e)=>{ console.log(e)});
    }
    this.deviceEnumerator.create().then(()=>{
      this.deviceEnumerator.addDeviceListChanged(async () => {
        callback(await this.deviceEnumerator.devices());
      });
    });
    
  }
  stopScan()
  {
    this.deviceEnumerator.removeDeviceListChanged();
    this.deviceEnumerator.close();
  }
}
export {DeviceManager, EventTypes, DeviceState};
