import * as React from 'react';
import {Platform,PermissionsAndroid} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import {DeviceManager, EventTypes, DeviceState} from './DeviceManager';

//-----Screens-----//
import HomeScreen from './screens/HomeScreen';
import DeviceSearchScreen from './screens/DeviceSearchScreen';
import SignalScreen from './screens/SignalScreen';
import ResistanceScreen from './screens/ResistanceScreen';

//-----UIComponents-----//
import {DeviceStatusBar} from './UIComponents/DeviceStatusBar';

const Stack = createNativeStackNavigator();

export async function requestPermissionAndroid()
{
  try {
    const granted = await PermissionsAndroid.requestMultiple([
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      PermissionsAndroid.PERMISSIONS.BLUETOOTH_CONNECT,
      PermissionsAndroid.PERMISSIONS.BLUETOOTH_SCAN
    ]
    )
    if (granted[PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION] === PermissionsAndroid.RESULTS.GRANTED) {
      console.log("permission access")
    } else {
      console.log("permission denied")
      requestPermissionAndroid();
    }
    if (granted[PermissionsAndroid.PERMISSIONS.BLUETOOTH_CONNECT] === PermissionsAndroid.RESULTS.GRANTED) {
      console.log("permission access")
    } else {
      console.log("permission denied")
      requestPermissionAndroid();
    }
    if (granted[PermissionsAndroid.PERMISSIONS.BLUETOOTH_SCAN] === PermissionsAndroid.RESULTS.GRANTED) {
      console.log("permission access")
    } else {
      console.log("permission denied")
      // requestPermissionAndroid();
    }
  } catch (err) {
    console.warn(err)
  }
}
class App extends React.Component 
{
  constructor(props) {
    super(props);
    if(Platform.OS === 'android'){
      requestPermissionAndroid();
    }
    this.DeviceManager = new DeviceManager();
    this.state = {deviceState: DeviceState.DISCONNECTED, powerProcent: "-" }
    this.DeviceManager.deviceManagerEmitter.addListener(EventTypes.DeviceStateChanged, (deviceState) => {
      console.log("APP "+ deviceState);
      this.setState({deviceState: deviceState})
      if(DeviceState.DISCONNECTED == deviceState){
        this.setState({powerProcent: "-"})
      }
    });
    this.DeviceManager.deviceManagerEmitter.addListener(EventTypes.DevicePowerProcentChanged, (powerProcent) => {
      this.setState({powerProcent: powerProcent})
    })
  }
  componentDidMount(){
    
  }
  
  render()
  {
     return <NavigationContainer>
        <Stack.Navigator initialRouteName="Home"
          screenOptions={{
            title: "BrainBit Demo",
            headerStyle: {
              backgroundColor: '#ffffff',
            },
            headerTintColor: '#000000',
            headerTitleStyle: {
              fontWeight: 'bold',
            },
          }}>
          <Stack.Screen name="Home" initialParams={{'deviceManager':this.DeviceManager}} deviceManager={this.DeviceManager} component={HomeScreen} />
          <Stack.Screen name="DeviceSearch" initialParams={{'deviceManager':this.DeviceManager}} component={DeviceSearchScreen} />
          <Stack.Screen name="Signal" initialParams={{'deviceManager':this.DeviceManager}} component={SignalScreen} />
          <Stack.Screen name="Resistance" initialParams={{'deviceManager':this.DeviceManager}} component={ResistanceScreen} />
        </Stack.Navigator>
        <DeviceStatusBar deviceState={this.state.deviceState} powerProcent={this.state.powerProcent}/>
      </NavigationContainer>    
  };
}

export default App;
