#include "cscanner.h"
#include "cdevice.h"
#include "cparams.h"
#include "sdk_error.h"
#include "stdio.h"

void connect_to_device(Device *device)
{
	int resultCode = device_connect(device);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot connect to device: %s\n", errorMsg);
		return;
	}

	DeviceState deviceState; 
	do
	{
		device_read_State(device, &deviceState);
	}
	while (deviceState != DeviceStateConnected);
	
	FirmwareVersion firmwareVersion;
	resultCode = device_read_FirmwareVersion(device, &firmwareVersion);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot read firmware version: %s\n", errorMsg);
		return;
	}

	printf("Firmware version: %d build %d\n", firmwareVersion.version, firmwareVersion.build);

	device_execute(device, CommandStartSignal);
		
	device_disconnect(device);
}

void on_device_found(DeviceEnumerator* enumerator, DeviceInfo device_info)
{
	Device* device = create_Device(enumerator, device_info);
	if (device == 0)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Device is null: %s\n", errorMsg);
		return;
	}
	
	char deviceName[256];
	int resultCode = device_read_Name(device, deviceName, 256);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Device read name error: %s\n", errorMsg);
		return;
	}

	char deviceAddress[256];
	resultCode = device_read_Address(device, deviceAddress, 256);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Device read address error: %s\n", errorMsg);
		return;
	}

	char deviceSerialNumber[256];
	resultCode = device_read_SerialNumber(device, deviceSerialNumber, 256);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Device read serial number error: %s\n", errorMsg);
		return;
	}
	
	DeviceState deviceState;
	resultCode = device_read_State(device, &deviceState);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Device read state error: %s\n", errorMsg);
		return;
	}

	printf
	(
		"Name: %s, MAC: [%s], S/N: %s, State: %s \n",
		deviceName,
		deviceAddress,
		deviceSerialNumber,
		deviceState == DeviceStateConnected ? "Connected" : "Disconnected"
	);

	connect_to_device(device);

	device_delete(device);
}

void on_device_list_changed(DeviceEnumerator* enumerator, void* user_data)
{
	DeviceInfoArray deviceInfoArray;
	int resultCode = enumerator_get_device_list(enumerator, &deviceInfoArray);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Device list read error: %s\n", errorMsg);
		return;
	}

	for (size_t i = 0; i < deviceInfoArray.info_count; ++i)
	{
		on_device_found(enumerator, deviceInfoArray.info_array[i]);
	}

	free_DeviceInfoArray(deviceInfoArray);
}

int main()
{
	DeviceEnumerator *enumerator = create_device_enumerator(DeviceTypeBrainbit);
	if (enumerator == NULL)
	{		
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Device enumerator is null: %s\n", errorMsg);
		return -1;
	}
	
	//store listener as long as you want to receive notifications
	DeviceListListener listListener;	
	int resultCode = enumerator_set_device_list_changed_callback(enumerator, &on_device_list_changed, &listListener, 0);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Subscription error: %s\n", errorMsg);
		enumerator_delete(enumerator);
		return -1;
	}

	//wait for any input
	getchar();

	enumerator_unsubscribe_device_list_changed(listListener);
	enumerator_delete(enumerator);
	return 0;
}