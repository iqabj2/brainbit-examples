#include "device_eeg.h"
#include "sdk_error.h"
#include "c_eeg_channels.h"
#include <stdio.h>
#include <string.h>
#include "utils.h"

#define SIGNAL_SAMPLES_COUNT 20

typedef struct _ChannelData {
    double data[SIGNAL_SAMPLES_COUNT];
    // The position of the data read cursor in the channel from the moment the channel was created
    size_t channelDataOffset;
    // data buffer is full
    bool dataFilled;
} ChannelData;

void data_eeg_channel_callback(AnyChannel* channel, size_t length, void* channelDataOut) {
    ChannelData* data = (ChannelData*)channelDataOut;
    if (!data->dataFilled && (length - data->channelDataOffset) >= SIGNAL_SAMPLES_COUNT) {
        size_t samples_read = 0;
        if (DoubleChannel_read_data((DoubleChannel*)channel, data->channelDataOffset, SIGNAL_SAMPLES_COUNT, data->data, SIGNAL_SAMPLES_COUNT, &samples_read) == SDK_NO_ERROR) {
            if (samples_read == SIGNAL_SAMPLES_COUNT) {
                data->channelDataOffset = data->channelDataOffset + samples_read;
                data->dataFilled = true;
            }
        }
    }
}

void device_eeg_mode(Device* device) {
    printf("------------[Device EEG]-----------\n");
    ChannelInfoArray deviceChannels;
    int resultCode = device_available_channels(device, &deviceChannels);
    if (resultCode != SDK_NO_ERROR) {
        char errorMsg[1024];
        sdk_last_error_msg(errorMsg, 1024);
        printf("Cannot get device channels info: %s\n", errorMsg);
        return;
    }

    EegDoubleChannel* T3Signal = NULL;
    EegDoubleChannel* T4Signal = NULL;
    EegDoubleChannel* O1Signal = NULL;
    EegDoubleChannel* O2Signal = NULL;
    for (size_t i = 0; i < deviceChannels.info_count; ++i) {
        if (deviceChannels.info_array[i].type == ChannelTypeSignal) {
            if (strcmp(deviceChannels.info_array[i].name, "T3") == 0) {
                T3Signal = create_EegDoubleChannel_info(device, deviceChannels.info_array[i]);
            } else if (strcmp(deviceChannels.info_array[i].name, "T4") == 0) {
                T4Signal = create_EegDoubleChannel_info(device, deviceChannels.info_array[i]);
            } else if (strcmp(deviceChannels.info_array[i].name, "O1") == 0) {
                O1Signal = create_EegDoubleChannel_info(device, deviceChannels.info_array[i]);
            } else if (strcmp(deviceChannels.info_array[i].name, "O2") == 0) {
                O2Signal = create_EegDoubleChannel_info(device, deviceChannels.info_array[i]);
            }
        }
    }
    free_ChannelInfoArray(deviceChannels);

    resultCode = device_execute(device, CommandStartSignal);
    if (resultCode != SDK_NO_ERROR) {
        char errorMsg[1024];
        sdk_last_error_msg(errorMsg, 1024);
        printf("Cannot execute StartSignal command: %s\n", errorMsg);
    } else {
        LengthListenerHandle T3SignalListener = NULL;
        LengthListenerHandle T4SignalListener = NULL;
        LengthListenerHandle O1SignalListener = NULL;
        LengthListenerHandle O2SignalListener = NULL;

        ChannelData T3Data;
        T3Data.channelDataOffset = 0;
        T3Data.dataFilled = false;
        ChannelData T4Data;
        T4Data.channelDataOffset = 0;
        T4Data.dataFilled = false;
        ChannelData O1Data;
        O1Data.channelDataOffset = 0;
        O1Data.dataFilled = false;
        ChannelData O2Data;
        O2Data.channelDataOffset = 0;
        O2Data.dataFilled = false;

        AnyChannel_add_length_callback((AnyChannel*)T3Signal, data_eeg_channel_callback, &T3SignalListener, &T3Data);
        AnyChannel_add_length_callback((AnyChannel*)T4Signal, data_eeg_channel_callback, &T4SignalListener, &T4Data);
        AnyChannel_add_length_callback((AnyChannel*)O1Signal, data_eeg_channel_callback, &O1SignalListener, &O1Data);
        AnyChannel_add_length_callback((AnyChannel*)O2Signal, data_eeg_channel_callback, &O2SignalListener, &O2Data);

        int attempts = 5;
        do {
            while (!T3Data.dataFilled
                   || !T4Data.dataFilled
                   || !O1Data.dataFilled
                   || !O2Data.dataFilled) {
                demo_sleep_ms(10);
            }

            printf("[%d signal samples are received.]\n", SIGNAL_SAMPLES_COUNT);
            printf("     T3:\t     T4:\t     O1:\t     O2:\n");
            for (size_t i = 0; i < SIGNAL_SAMPLES_COUNT; ++i) {
                printf("%.2fuV\t%.2fuV\t%.2fuV\t%.2fuV\n",
                       T3Data.data[i] * 1e6,
                       T4Data.data[i] * 1e6,
                       O1Data.data[i] * 1e6,
                       O2Data.data[i] * 1e6);
            }
            // Reset data
            T3Data.dataFilled = false;
            T4Data.dataFilled = false;
            O1Data.dataFilled = false;
            O2Data.dataFilled = false;
        } while (attempts-- > 0);

        resultCode = device_execute(device, CommandStopSignal);
        if (resultCode != SDK_NO_ERROR) {
            char errorMsg[1024];
            sdk_last_error_msg(errorMsg, 1024);
            printf("Cannot execute StopSignal command: %s\n", errorMsg);
        }

        free_length_listener_handle(T3SignalListener);
        free_length_listener_handle(T4SignalListener);
        free_length_listener_handle(O1SignalListener);
        free_length_listener_handle(O2SignalListener);
    }

    AnyChannel_delete((AnyChannel*)T3Signal);
    AnyChannel_delete((AnyChannel*)T4Signal);
    AnyChannel_delete((AnyChannel*)O1Signal);
    AnyChannel_delete((AnyChannel*)O2Signal);

    printf("-----------------------------------\n");
}