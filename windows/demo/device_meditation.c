#include "device_meditation.h"
#include "sdk_error.h"
#include "c_eeg_channels.h"
#include <stdio.h>
#include <string.h>
#include "utils.h"

typedef struct _ChannelData {
    int level;
    int levelProgress;
    bool levelChanged;
    bool levelProgressChanged;
} ChannelData;

void data_meditation_level_channel_callback(MeditationAnalyzer* channel, int level, void* channelDataOut) {
    ChannelData* data = (ChannelData*)channelDataOut;
    data->levelProgress = level;
    data->levelChanged = true;
}
void data_meditation_level_progress_channel_callback(MeditationAnalyzer* channel, double levelProgress, void* channelDataOut) {
    ChannelData* data = (ChannelData*)channelDataOut;
    data->levelProgress = (int)(levelProgress * 100.0); // convert to percent 0..100
    data->levelProgressChanged = true;
}

void device_meditation_mode(Device* device) {
    printf("---------[Device meditation]-------\n");
    ChannelInfoArray deviceChannels;
    int resultCode = device_available_channels(device, &deviceChannels);
    if (resultCode != SDK_NO_ERROR) {
        char errorMsg[1024];
        sdk_last_error_msg(errorMsg, 1024);
        printf("Cannot get device channels info: %s\n", errorMsg);
        return;
    }

    EegDoubleChannel* T3Signal = NULL;
    EegDoubleChannel* T4Signal = NULL;
    EegDoubleChannel* O1Signal = NULL;
    EegDoubleChannel* O2Signal = NULL;
    for (size_t i = 0; i < deviceChannels.info_count; ++i) {
        if (deviceChannels.info_array[i].type == ChannelTypeSignal) {
            if (strcmp(deviceChannels.info_array[i].name, "T3") == 0) {
                T3Signal = create_EegDoubleChannel_info(device, deviceChannels.info_array[i]);
            } else if (strcmp(deviceChannels.info_array[i].name, "T4") == 0) {
                T4Signal = create_EegDoubleChannel_info(device, deviceChannels.info_array[i]);
            } else if (strcmp(deviceChannels.info_array[i].name, "O1") == 0) {
                O1Signal = create_EegDoubleChannel_info(device, deviceChannels.info_array[i]);
            } else if (strcmp(deviceChannels.info_array[i].name, "O2") == 0) {
                O2Signal = create_EegDoubleChannel_info(device, deviceChannels.info_array[i]);
            }
        }
    }
    free_ChannelInfoArray(deviceChannels);
    EegIndexChannel* eegIndexChannel = create_EegIndexChannel(T3Signal, T4Signal, O1Signal, O2Signal);
    MeditationAnalyzer* meditationAnalyzer = create_MeditationAnalyzer(eegIndexChannel);

    resultCode = device_execute(device, CommandStartSignal);
    if (resultCode != SDK_NO_ERROR) {
        char errorMsg[1024];
        sdk_last_error_msg(errorMsg, 1024);
        printf("Cannot execute StartSignal command: %s\n", errorMsg);
    } else {
        MeditationLevelListener meditationLevelListener = NULL;
        MeditationLevelProgressListener meditationLevelProgressListener = NULL;
        ChannelData meditationData;
        meditationData.level = 0;
        meditationData.levelProgress = 0;
        meditationData.levelChanged = false;
        meditationData.levelProgressChanged = false;
        MeditationAnalyzer_set_level_changed_callback(meditationAnalyzer, data_meditation_level_channel_callback, &meditationLevelListener, &meditationData);
        MeditationAnalyzer_set_level_progress_changed_callback(meditationAnalyzer, data_meditation_level_progress_channel_callback, &meditationLevelProgressListener, &meditationData);

        printf("Level: [0]\n");
        printf("Level progress: [0]\n");
        printf("May take a long time...\n");
        int attempts = 5;
        do {
            while (!meditationData.levelChanged && !meditationData.levelProgressChanged) {
                demo_sleep_ms(250);
            }
            if (meditationData.levelChanged) {
                meditationData.levelChanged = false;
                printf("Level: [%d]\n", meditationData.level);
            }
            if (meditationData.levelProgressChanged) {
                meditationData.levelProgressChanged = false;
                printf("Level progress: [%d]\n", meditationData.levelProgress);
            }
        } while (attempts-- > 0);

        resultCode = device_execute(device, CommandStopSignal);
        if (resultCode != SDK_NO_ERROR) {
            char errorMsg[1024];
            sdk_last_error_msg(errorMsg, 1024);
            printf("Cannot execute StopSignal command: %s\n", errorMsg);
        }

        free_MeditationLevelListener(meditationLevelListener);
        free_MeditationLevelProgressListener(meditationLevelProgressListener);
    }
    MeditationAnalyzer_delete(meditationAnalyzer);
    AnyChannel_delete((AnyChannel*)eegIndexChannel);
    AnyChannel_delete((AnyChannel*)T3Signal);
    AnyChannel_delete((AnyChannel*)T4Signal);
    AnyChannel_delete((AnyChannel*)O1Signal);
    AnyChannel_delete((AnyChannel*)O2Signal);

    printf("-----------------------------------\n");
}