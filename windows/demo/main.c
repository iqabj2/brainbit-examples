#include "cdevice.h"
#include "sdk_error.h"
#include "utils.h"
#include "device_finder.h"
#include "device_info.h"
#include "device_battery.h"
#include "device_resistance.h"
#include "device_signal.h"
#include "device_eeg.h"
#include "device_eeg_index.h"
#include "device_meditation.h"
#include "device_emotions.h"

#include "stdio.h"



bool dev_connect(Device* device) {
    const int resultCode = device_connect(device);
    if (resultCode != SDK_NO_ERROR) {
        char errorMsg[1024];
        sdk_last_error_msg(errorMsg, 1024);
        printf("Cannot connect to device: %s\n", errorMsg);
        return false;
    }
    return true;
}

int main() {
    while (true) {
        printf("Enter a serial number of a device or \"q\" to exit.\n");

        uint64_t serialNumber;
        int resCode = read_console_value(&serialNumber);
        if (resCode == 0) { // ok. Process serial number
            Device* device = find_device_uses_callback(serialNumber);
            if (device != NULL) {
                if (dev_connect(device)) {
                    DemoMode mode;
                    resCode = read_demo_mode_number(&mode);
                    if (resCode == 0) {
                        switch (mode) {
                            case DemoModeDeviceInfo:
                            {
                                device_info_mode(device);
                                break;
                            }
                            case DemoModeDeviceBattery:
                            {
                                device_battery_mode(device);
                                break;
                            }
                            case DemoModeDeviceResistance:
                            {
                                device_resistance_mode(device);
                                break;
                            }
                            case DemoModeDeviceSignal:
                            {
                                device_signal_mode(device);
                                break;
                            }
                            case DemoModeDeviceEEG:
                            {
                                device_eeg_mode(device);
                                break;
                            }
                            case DemoModeDeviceEEGIndex:
                            {
                                device_eeg_index_mode(device);
                                break;
                            }
                            case DemoModeDeviceMeditation:
                            {
                                device_meditation_mode(device);
                                break;
                            }
                            case DemoModeDeviceEmotions:
                            {
                                device_emotions_mode(device);
                                break;
                            }
                        }
                    }
                    device_disconnect(device);
                    device_delete(device);
                    if (resCode > 1) { // Exit
                        break;
                    }
                } else {
                    device_delete(device);
                }
            }
        } else if (resCode == 1) { // continue
            continue;
        } else { // Exit
            break;
        }
    }
    return 0;
}