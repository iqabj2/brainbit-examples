#include "device_eeg_index.h"
#include "sdk_error.h"
#include "c_eeg_channels.h"
#include <stdio.h>
#include <string.h>
#include "utils.h"

#define SIGNAL_SAMPLES_COUNT 20

typedef struct _ChannelData {
    EegIndexValues data[SIGNAL_SAMPLES_COUNT];
    // The position of the data read cursor in the channel from the moment the channel was created
    size_t channelDataOffset;
    // data buffer is full
    bool dataFilled;
} ChannelData;

void data_eeg_index_channel_callback(AnyChannel* channel, size_t length, void* channelDataOut) {
    ChannelData* data = (ChannelData*)channelDataOut;
    if (!data->dataFilled && (length - data->channelDataOffset) >= SIGNAL_SAMPLES_COUNT) {
        size_t samples_read = 0;
        if (EegIndexChannel_read_data((EegIndexChannel*)channel, data->channelDataOffset, SIGNAL_SAMPLES_COUNT, data->data, SIGNAL_SAMPLES_COUNT, &samples_read) == SDK_NO_ERROR) {
            if (samples_read == SIGNAL_SAMPLES_COUNT) {
                data->channelDataOffset = data->channelDataOffset + samples_read;
                data->dataFilled = true;
            }
        }
    }
}

void device_eeg_index_mode(Device* device) {
    printf("---------[Device EEG Index]--------\n");
    ChannelInfoArray deviceChannels;
    int resultCode = device_available_channels(device, &deviceChannels);
    if (resultCode != SDK_NO_ERROR) {
        char errorMsg[1024];
        sdk_last_error_msg(errorMsg, 1024);
        printf("Cannot get device channels info: %s\n", errorMsg);
        return;
    }

    EegDoubleChannel* T3Signal = NULL;
    EegDoubleChannel* T4Signal = NULL;
    EegDoubleChannel* O1Signal = NULL;
    EegDoubleChannel* O2Signal = NULL;
    for (size_t i = 0; i < deviceChannels.info_count; ++i) {
        if (deviceChannels.info_array[i].type == ChannelTypeSignal) {
            if (strcmp(deviceChannels.info_array[i].name, "T3") == 0) {
                T3Signal = create_EegDoubleChannel_info(device, deviceChannels.info_array[i]);
            } else if (strcmp(deviceChannels.info_array[i].name, "T4") == 0) {
                T4Signal = create_EegDoubleChannel_info(device, deviceChannels.info_array[i]);
            } else if (strcmp(deviceChannels.info_array[i].name, "O1") == 0) {
                O1Signal = create_EegDoubleChannel_info(device, deviceChannels.info_array[i]);
            } else if (strcmp(deviceChannels.info_array[i].name, "O2") == 0) {
                O2Signal = create_EegDoubleChannel_info(device, deviceChannels.info_array[i]);
            }
        }
    }
    free_ChannelInfoArray(deviceChannels);
    EegIndexChannel* eegIndexChannel = create_EegIndexChannel(T3Signal, T4Signal, O1Signal, O2Signal);

    resultCode = device_execute(device, CommandStartSignal);
    if (resultCode != SDK_NO_ERROR) {
        char errorMsg[1024];
        sdk_last_error_msg(errorMsg, 1024);
        printf("Cannot execute StartSignal command: %s\n", errorMsg);
    } else {
        LengthListenerHandle eegIndexListener = NULL;
        ChannelData eegIndexData;
        eegIndexData.channelDataOffset = 0;
        eegIndexData.dataFilled = false;
        AnyChannel_add_length_callback((AnyChannel*)eegIndexChannel, data_eeg_index_channel_callback, &eegIndexListener, &eegIndexData);

        int attempts = 5;
        do {
            while (!eegIndexData.dataFilled) {
                demo_sleep_ms(10);
            }
            double alphaRateAvg = 0;
            double betaRateAvg = 0;
            double deltaRateAvg = 0;
            double thetaRateAvg = 0;
            for (size_t i = 0; i < SIGNAL_SAMPLES_COUNT; ++i) {
                alphaRateAvg += eegIndexData.data[i].AlphaRate;
                betaRateAvg += eegIndexData.data[i].BetaRate;
                deltaRateAvg += eegIndexData.data[i].DeltaRate;
                thetaRateAvg += eegIndexData.data[i].ThetaRate;
            }
            eegIndexData.dataFilled = false;
            printf("Average EEG Index [alpha]:[%.2f] [beta]:[%.2f] [delta]:[%.2f] [theta]:[%.2f]\n",
                   alphaRateAvg / SIGNAL_SAMPLES_COUNT,
                   betaRateAvg / SIGNAL_SAMPLES_COUNT,
                   deltaRateAvg / SIGNAL_SAMPLES_COUNT,
                   thetaRateAvg / SIGNAL_SAMPLES_COUNT);
        } while (attempts-- > 0);

        resultCode = device_execute(device, CommandStopSignal);
        if (resultCode != SDK_NO_ERROR) {
            char errorMsg[1024];
            sdk_last_error_msg(errorMsg, 1024);
            printf("Cannot execute StopSignal command: %s\n", errorMsg);
        }

        free_length_listener_handle(eegIndexListener);
    }

    AnyChannel_delete((AnyChannel*)eegIndexChannel);
    AnyChannel_delete((AnyChannel*)T3Signal);
    AnyChannel_delete((AnyChannel*)T4Signal);
    AnyChannel_delete((AnyChannel*)O1Signal);
    AnyChannel_delete((AnyChannel*)O2Signal);

    printf("-----------------------------------\n");
}