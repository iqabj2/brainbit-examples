#include "device_resistance.h"
#include "cresistance-channel.h"
#include "sdk_error.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "utils.h"
#include <math.h>

#define RESISTANCE_AVERAGE_COUNT 10
#define RESISTANCE_SKIPED_COUNT 10

typedef struct _AverageResistance {
	double Value;
	size_t AverageCount;
	size_t SkipCount;
} AverageResistance;

void append_resistance_value(AverageResistance* resistance, DoubleDataArray resistance_array) {
	for (size_t i = 0; i < resistance_array.samples_count && resistance->AverageCount < RESISTANCE_AVERAGE_COUNT; ++i) {
		if (fpclassify(resistance_array.data_array[i]) != FP_INFINITE) {
			++resistance->AverageCount;
			resistance->Value += resistance_array.data_array[i] / RESISTANCE_AVERAGE_COUNT;
		}
		else
		{
			++resistance->SkipCount;
		}
	}
}

void resistance_callback(Device* device, ChannelInfo channel_info, DoubleDataArray data_array, void* resistance) {
	if (channel_info.type == ChannelTypeResistance) {
		//accumulating RESISTANCE_AVERAGE_COUNT resistance values for each channel
		if (strcmp(channel_info.name, "T3") == 0) {
			append_resistance_value((AverageResistance*)resistance, data_array);
		}
		if (strcmp(channel_info.name, "T4") == 0) {
			append_resistance_value((AverageResistance*)resistance, data_array);
		}
		if (strcmp(channel_info.name, "O1") == 0) {
			append_resistance_value((AverageResistance*)resistance, data_array);
		}
		if (strcmp(channel_info.name, "O2") == 0) {
			append_resistance_value((AverageResistance*)resistance, data_array);
		}
	}
	free_DoubleDataArray(data_array);
}

void device_resistance_mode(Device* device) {
	printf("----------[Resistance]-------------\n");
	ChannelInfoArray channels;
	int resultCode = device_available_channels(device, &channels);
	if (resultCode == SDK_NO_ERROR) {
		DoubleDataListenerHandle T3ResistanceListener = NULL;
		DoubleDataListenerHandle T4ResistanceListener = NULL;
		DoubleDataListenerHandle O1ResistanceListener = NULL;
		DoubleDataListenerHandle O2ResistanceListener = NULL;
		AverageResistance T3Resistance;
		AverageResistance T4Resistance;
		AverageResistance O1Resistance;
		AverageResistance O2Resistance;
		T3Resistance.AverageCount = 0;
		T3Resistance.SkipCount = 0;
		T3Resistance.Value = 0.0;
		T4Resistance.AverageCount = 0;
		T4Resistance.SkipCount = 0;
		T4Resistance.Value = 0.0;
		O1Resistance.AverageCount = 0;
		O1Resistance.SkipCount = 0;
		O1Resistance.Value = 0.0;
		O2Resistance.AverageCount = 0;
		O2Resistance.SkipCount = 0;
		O2Resistance.Value = 0.0;

		for (size_t i = 0; i < channels.info_count; ++i) {
			const ChannelInfo chInf = channels.info_array[i];
			if (chInf.type == ChannelTypeResistance) {
				if (strcmp(chInf.name, "T3") == 0) {
					device_subscribe_double_channel_data_received(device, chInf, &resistance_callback, &T3ResistanceListener, &T3Resistance);
				}
				else if (strcmp(chInf.name, "T4") == 0) {
					device_subscribe_double_channel_data_received(device, chInf, &resistance_callback, &T4ResistanceListener, &T4Resistance);
				}
				else if (strcmp(chInf.name, "O1") == 0) {
					device_subscribe_double_channel_data_received(device, chInf, &resistance_callback, &O1ResistanceListener, &O1Resistance);
				}
				else if (strcmp(chInf.name, "O2") == 0) {
					device_subscribe_double_channel_data_received(device, chInf, &resistance_callback, &O2ResistanceListener, &O2Resistance);
				}
			}
		}
		free_ChannelInfoArray(channels);
		resultCode = device_execute(device, CommandStartResist);
		if (resultCode != SDK_NO_ERROR) {
			char errorMsg[1024];
			sdk_last_error_msg(errorMsg, 1024);
			printf("Cannot execute StartResist command: %s\n", errorMsg);
		}
		else {
			while ((T3Resistance.AverageCount < RESISTANCE_AVERAGE_COUNT
				&& T4Resistance.AverageCount < RESISTANCE_AVERAGE_COUNT
				&& O1Resistance.AverageCount < RESISTANCE_AVERAGE_COUNT
				&& O2Resistance.AverageCount < RESISTANCE_AVERAGE_COUNT)
				&& (T3Resistance.SkipCount <= RESISTANCE_SKIPED_COUNT
					&& T4Resistance.SkipCount <= RESISTANCE_SKIPED_COUNT
					&& O1Resistance.SkipCount <= RESISTANCE_SKIPED_COUNT
					&& O2Resistance.SkipCount <= RESISTANCE_SKIPED_COUNT)
				) {
				//waiting for resistance
				demo_sleep_ms(10);
			}
			resultCode = device_execute(device, CommandStopResist);
			if (resultCode != SDK_NO_ERROR) {
				char errorMsg[1024];
				sdk_last_error_msg(errorMsg, 1024);
				printf("Cannot execute StopResist command: %s\n", errorMsg);
			}
			free_double_data_listener_handle(T3ResistanceListener);
			free_double_data_listener_handle(T4ResistanceListener);
			free_double_data_listener_handle(O1ResistanceListener);
			free_double_data_listener_handle(O2ResistanceListener);

			//printing resistance in kOhms
			printf("Average resistance [T3]:[%.2f kOhm] [T4]:[%.2f kOhm] [O1]:[%.2f kOhm] [O2]:[%.2f kOhm]\n",
				T3Resistance.AverageCount > 0 ? T3Resistance.Value / 1e3 : INFINITY,
				T4Resistance.AverageCount > 0 ? T4Resistance.Value / 1e3 : INFINITY,
				O1Resistance.AverageCount > 0 ? O1Resistance.Value / 1e3 : INFINITY,
				O2Resistance.AverageCount > 0 ? O2Resistance.Value / 1e3 : INFINITY);
		}
	}
	else {
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot get device channels info: %s\n", errorMsg);
	}
	printf("-----------------------------------\n");
}