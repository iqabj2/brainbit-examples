#include "utils.h"
#include "stdio.h"

int read_console_value(uint64_t* val) {
    //if (scanf_s("%llu", &serialNumber, (rsize_t)sizeof(uint64_t)) == 0)
    if (scanf_s("%llu", val) == 0) {
        if (fgetc(stdin) == 'q') {
            while (fgetc(stdin) != '\n') {}
            return 2;
        } else {
            while (fgetc(stdin) != '\n') {}
            printf("Invalid command\n");
            return 1;
        }
    }
    return 0;
}

int read_demo_mode_number(DemoMode* mode) {
    printf("-----------------------------------\n");
    printf("[1] Device info\n");
    printf("[2] Device battery\n");
    printf("[3] Device resistance\n");
    printf("[4] Device raw signal\n");
    printf("[5] Device EEG signal\n");
    printf("[6] Device EEG Index\n");
    printf("[7] Device Meditation\n");
    printf("[8] Device Emotions\n");
    printf("-----------------------------------\n");
    printf("Enter a number of demo mode or \"q\" to exit.\n");
    uint64_t val;
    int resCode = read_console_value(&val);
    if (resCode == 0) {
        switch (val) {
            case 1:
            {
                (*mode) = DemoModeDeviceInfo;
                break;
            }
            case 2:
            {
                (*mode) = DemoModeDeviceBattery;
                break;
            }
            case 3:
            {
                (*mode) = DemoModeDeviceResistance;
                break;
            }
            case 4:
            {
                (*mode) = DemoModeDeviceSignal;
                break;
            }
            case 5:
            {
                (*mode) = DemoModeDeviceEEG;
                break;
            }
            case 6:
            {
                (*mode) = DemoModeDeviceEEGIndex;
                break;
            }
            case 7:
            {
                (*mode) = DemoModeDeviceMeditation;
                break;
            }
            case 8:
            {
                (*mode) = DemoModeDeviceEmotions;
                break;
            }
            default:
                (*mode) = DemoModeUnknown;
                printf("Invalid mode\n");
                return 1;
        }
    }
    return resCode;
}


#ifdef WIN32
#include <windows.h>
#include <synchapi.h>
void demo_sleep_ms(uint32_t ms) {
    Sleep(ms);
}
#else
#include <unistd.h>
void demo_sleep_ms(uint32_t ms) {
    usleep(ms);
}
#endif // WIN32
//MutexSimple create_mutex() {
//    return CreateMutex(
//        NULL,              // default security attributes
//        FALSE,             // initially not owned
//        NULL);             // unnamed mutex
//}
//
//void free_mutex(MutexSimple* mutex) {
//    CloseHandle(*mutex);
//}
//
//bool lock_mutex(MutexSimple* mutex) {
//    return WaitForSingleObject(
//        *mutex,    // handle to mutex
//        INFINITE) == WAIT_OBJECT_0;  // no time-out interval
//}
//
//bool unlcok_mutex(MutexSimple* mutex) {
//    return ReleaseMutex(*mutex);
//}