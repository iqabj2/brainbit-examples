#include "device_emotions.h"
#include "sdk_error.h"
#include "cresistance-channel.h"
#include "c_eeg_channels.h"
#include <stdio.h>
#include <string.h>
#include "utils.h"

typedef struct _EmotionsChannelData {
	EmotionsSample emotionsData;
	bool dataChanged;
} EmotionsChannelData;

typedef struct _CallibrationData {
	bool calibrated;
	bool calibrationProgressUp;
	float calibrationProgress;
} CallibrationData;

void data_emotion_state_channel_callback(EmotionsAnalyzer* channel, EmotionsSample sample, void* channelDataOut) {
	EmotionsChannelData* data = (EmotionsChannelData*)channelDataOut;
	data->dataChanged = true;
	data->emotionsData = sample;
}

void data_analyzer_state_channel_callback(EmotionsAnalyzer* channel, EmotionsAnalyzerState analyzerState, CalibrationStatus calibrationStatus, void* channelDataOut) {
	CallibrationData* data = (CallibrationData*)channelDataOut;
	if (analyzerState == StateWorking) {
		data->calibrated = true;
	}
	else {
		data->calibrationProgressUp = true;
		data->calibrationProgress = calibrationStatus.Progress;
	}
}

void device_emotions_mode(Device* device) 
{
    printf("---------[Device emotions]-------\n");

	EmotionsAnalyzer* emotionAnalyzer = create_EmotionsAnalyzer(device);

    //get resisance value
	ChannelInfoArray channels;
	int resultCode = device_available_channels(device, &channels);
	if (resultCode == SDK_NO_ERROR) {
		ResistanceDoubleChannel* o1ResistChannel = NULL;
		ResistanceDoubleChannel* o2ResistChannel = NULL;
		ResistanceDoubleChannel* t3ResistChannel = NULL;
		ResistanceDoubleChannel* t4ResistChannel = NULL;

		for (size_t i = 0; i < channels.info_count; ++i) {
			const ChannelInfo chInf = channels.info_array[i];
			if (chInf.type == ChannelTypeResistance) {
				if (strcmp(chInf.name, "T3") == 0) {
					t3ResistChannel = create_ResistanceDoubleChannel_info(device, chInf);
				}
				else if (strcmp(chInf.name, "T4") == 0) {
					t4ResistChannel = create_ResistanceDoubleChannel_info(device, chInf);
				}
				else if (strcmp(chInf.name, "O1") == 0) {
					o1ResistChannel = create_ResistanceDoubleChannel_info(device, chInf);
				}
				else if (strcmp(chInf.name, "O2") == 0) {
					o2ResistChannel = create_ResistanceDoubleChannel_info(device, chInf);
				}
			}
		}
		free_ChannelInfoArray(channels);

		resultCode = device_execute(device, CommandStartResist);
		if (resultCode != SDK_NO_ERROR) {
			char errorMsg[1024];
			sdk_last_error_msg(errorMsg, 1024);
			printf("Cannot execute StartResist command: %s\n", errorMsg);
		}
		else {
			printf("Wait resistance value for 10 sec...\n");
			demo_sleep_ms(10000);

			size_t t3Length = 0;
			AnyChannel_get_total_length(t3ResistChannel, &t3Length);
			double t3data[1];
			DoubleChannel_read_data(t3ResistChannel, t3Length - 1, t3Length, &t3data, 1, 1);

			size_t t4Length = 0;
			AnyChannel_get_total_length(t4ResistChannel, &t4Length);
			double t4data[1];
			DoubleChannel_read_data(t4ResistChannel, t4Length - 1, t4Length, &t4data, 1, 1);

			size_t o1Length = 0;
			AnyChannel_get_total_length(o1ResistChannel, &o1Length);
			double o1data[1];
			DoubleChannel_read_data(o1ResistChannel, o1Length - 1, o1Length, &o1data, 1, 1);

			size_t o2Length = 0;
			AnyChannel_get_total_length(o1ResistChannel, &o2Length);
			double o2data[1];
			DoubleChannel_read_data(o2ResistChannel, o2Length - 1, o2Length, &o2data, 1, 1);

			if (t3data[0] > 2e6 || t4data[0] > 2e6 || o1data[0] > 2e6 || o2data[0] > 2e6)
			{
				if ((t3data[0] > 2e6 || o1data[0] > 2e6) && (t4data[0] < 2e6 && o2data[0] < 2e6))
				{
					EmotionsAnalyzer_set_priority_side(emotionAnalyzer, PrioritySideRight);
				}
				else if ((t4data[0] > 2e6 || o2data[0] > 2e6) && (t3data[0] < 2e6 && o1data[0] < 2e6))
				{
					EmotionsAnalyzer_set_priority_side(emotionAnalyzer, PrioritySideLeft);
				}
			}

			resultCode = device_execute(device, CommandStopResist);
			if (resultCode != SDK_NO_ERROR) {
				char errorMsg[1024];
				sdk_last_error_msg(errorMsg, 1024);
				printf("Cannot execute StopResist command: %s\n", errorMsg);
			}

			AnyChannel_delete((AnyChannel*)t3ResistChannel);
			AnyChannel_delete((AnyChannel*)t4ResistChannel);
			AnyChannel_delete((AnyChannel*)o1ResistChannel);
			AnyChannel_delete((AnyChannel*)o2ResistChannel);
		}
	}

	resultCode = EmotionsAnalyzer_set_weights(emotionAnalyzer, 0, 0, 1, 1);
	if (resultCode != SDK_NO_ERROR) {
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot set weights to emotion channel: %s\n", errorMsg);
	}
	resultCode = device_execute(device, CommandStartSignal);
	if (resultCode != SDK_NO_ERROR) {
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot execute StartSignal command: %s\n", errorMsg);
	}
	else {
		EmotionalStateListener stateListener = NULL;
		EmotionsAnalyzerStateListener analyzerStateListener = NULL;

		EmotionsChannelData emotionsData;
		emotionsData.dataChanged = false;
		CallibrationData calibrationData;
		calibrationData.calibrated = false;
		calibrationData.calibrationProgressUp = false;
		calibrationData.calibrationProgress = 0;

		EmotionsAnalyzer_set_emotional_state_callback(emotionAnalyzer, data_emotion_state_channel_callback, &stateListener, &emotionsData);
		EmotionsAnalyzer_set_analyzer_state_callback(emotionAnalyzer, data_analyzer_state_channel_callback, &analyzerStateListener, &calibrationData);

		resultCode = EmotionsAnalyzer_calibrate(emotionAnalyzer);
		if (resultCode != SDK_NO_ERROR) {
			char errorMsg[1024];
			sdk_last_error_msg(errorMsg, 1024);
			printf("Cannot start calibration: %s\n", errorMsg);
		}

		printf("Calibration started...\n");
		bool calibrated = false;
		do {
			while (!calibrationData.calibrationProgressUp) {
				demo_sleep_ms(100);
			}

			if (calibrationData.calibrationProgressUp) {
				calibrationData.calibrationProgressUp = false;
				printf("Calibration progress: %.3f\n", calibrationData.calibrationProgress);
			}
			calibrated = calibrationData.calibrated;
		} while (!calibrated);

		int attempts = 5;
		do {
			while (!emotionsData.dataChanged) {
				demo_sleep_ms(250);
			}
			emotionsData.dataChanged = false;
			printf("Alpha: %.2f\nBeta: %.2f\nDelta: %.2f\nTheta: %.2f\nRelaxation: %.2f\nConcentration: %.2f\n\n",
				emotionsData.emotionsData.AlphaRate,
				emotionsData.emotionsData.BetaRate,
				emotionsData.emotionsData.DeltaRate,
				emotionsData.emotionsData.ThetaRate,
				emotionsData.emotionsData.RelaxationRate,
				emotionsData.emotionsData.ConcentrationRate);
		} while (attempts-- > 0);

		resultCode = device_execute(device, CommandStopSignal);
		if (resultCode != SDK_NO_ERROR) {
			char errorMsg[1024];
			sdk_last_error_msg(errorMsg, 1024);
			printf("Cannot execute StopSignal command: %s\n", errorMsg);
		}

		free_EmotionalStateListener(stateListener);
		free_EmotionsAnalyzerStateListener(analyzerStateListener);
	}
	EmotionsAnalyzer_delete(emotionAnalyzer);
	
}