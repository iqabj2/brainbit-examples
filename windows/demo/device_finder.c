#include "device_finder.h"
#include "utils.h"
#include "sdk_error.h"

#include <stdio.h>
#include <stdbool.h>


//#include <synchapi.h>

typedef struct _EnumeratorResult {
    //MutexSimple mutexDevInf;
    DeviceInfo deviceInfo;
    bool founded;
    uint64_t serialNumberRequired;
} EnumeratorResult;

void enumerator_callback(DeviceEnumerator* enumerator, void* data) {
    DeviceInfoArray deviceInfoArray;
    const int resultCode = enumerator_get_device_list(enumerator, &deviceInfoArray);
    if (resultCode != SDK_NO_ERROR) {
        return;
    }
    if (deviceInfoArray.info_count > 0) {
        EnumeratorResult* enResult = (EnumeratorResult*)data;
        for (size_t i = 0; i < deviceInfoArray.info_count; ++i) {
            if (deviceInfoArray.info_array[i].SerialNumber == enResult->serialNumberRequired) {
                enResult->deviceInfo = deviceInfoArray.info_array[i];
                enResult->founded = true;
                break;
            }
        }
    }
    free_DeviceInfoArray(deviceInfoArray);
}

Device* find_device_uses_callback(uint64_t serialNumber) {
    DeviceEnumerator* enumerator = create_device_enumerator(DeviceTypeBrainbitAny);
    if (enumerator == NULL) {
        char errorMsg[1024];
        sdk_last_error_msg(errorMsg, 1024);
        printf("Device enumerator is null: %s\n", errorMsg);
        return NULL;
    }
    DeviceListListener listener;
    EnumeratorResult enResult;
    enResult.serialNumberRequired = serialNumber;
    enResult.founded = false;

    enumerator_set_device_list_changed_callback(enumerator, enumerator_callback, &listener, &enResult);

    int attempts = 10;
    bool founded = false;
    do {
        demo_sleep_ms(500);
        if (enResult.founded)
            break;
    } while (attempts-- > 0);
    Device* outDeviceInfo = NULL;
    enumerator_unsubscribe_device_list_changed(listener);
    if (enResult.founded) {
        printf("Device with SN %llu found. Name: %s, Address: %s\n", serialNumber, enResult.deviceInfo.Name, enResult.deviceInfo.Address);
        outDeviceInfo = create_Device(enumerator, enResult.deviceInfo);
    } else {
        printf("Device with SN %llu not found.\n", serialNumber);
    }
    enumerator_delete(enumerator);
    return outDeviceInfo;
}