#ifndef DEVICE_RESISTANCE_H
#define DEVICE_RESISTANCE_H

#include "cdevice.h"

void device_resistance_mode(Device* device);

#endif // DEVICE_RESISTANCE_H