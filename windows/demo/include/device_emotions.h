#ifndef DEVICE_EMOTIONS_H
#define DEVICE_EMOTIONS_H

#include "cdevice.h"

void device_emotions_mode(Device* device);

#endif // DEVICE_EMOTIONS_H