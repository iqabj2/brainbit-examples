#ifndef DEVICE_EEG_INDEX_H
#define DEVICE_EEG_INDEX_H

#include "cdevice.h"

void device_eeg_index_mode(Device* device);

#endif // DEVICE_EEG_INDEX_H