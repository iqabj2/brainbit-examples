#ifndef MAIN_UTILS_H
#define MAIN_UTILS_H

#include <stdint.h>

typedef enum _DemoMode {
    DemoModeUnknown,
    DemoModeDeviceInfo,
    DemoModeDeviceBattery,
    DemoModeDeviceResistance,
    DemoModeDeviceSignal,
    DemoModeDeviceEEG,
    DemoModeDeviceEEGIndex,
    DemoModeDeviceMeditation,
    DemoModeDeviceEmotions,
} DemoMode;

int read_demo_mode_number(DemoMode*);
int read_console_value(uint64_t*);
void demo_sleep_ms(uint32_t);

#endif // MAIN_UTILS_H