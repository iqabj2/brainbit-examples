#ifndef DEVICE_MEDITATION_H
#define DEVICE_MEDITATION_H

#include "cdevice.h"

void device_meditation_mode(Device* device);

#endif // DEVICE_MEDITATION_H