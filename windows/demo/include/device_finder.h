#ifndef DEVICE_FINDER_H
#define DEVICE_FINDER_H

#include "cdevice.h"

Device* find_device_uses_callback(uint64_t serialNumber);

#endif // DEVICE_FINDER_H