#include "cscanner.h"
#include "cdevice.h"
#include "sdk_error.h"
#include "stdio.h"
#include "windows.h"

int find_device_info(DeviceEnumerator* enumerator, uint64_t serial_number, DeviceInfo *out_device_info)
{
	DeviceInfoArray deviceInfoArray;
	const int resultCode = enumerator_get_device_list(enumerator, &deviceInfoArray);
	if (resultCode != SDK_NO_ERROR)
	{
		return -1;
	}
	
	for (size_t i = 0; i < deviceInfoArray.info_count; ++i)
	{
		if (deviceInfoArray.info_array[i].SerialNumber == serial_number)
		{
			*out_device_info = deviceInfoArray.info_array[i];
			free_DeviceInfoArray(deviceInfoArray);
			return 0;
		}
	}

	free_DeviceInfoArray(deviceInfoArray);
	return -1;
}

void find_by_serial(uint64_t serial_number)
{
	DeviceEnumerator* enumerator = create_device_enumerator(DeviceTypeBrainbit);
	if (enumerator == NULL)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Device enumerator is null: %s\n", errorMsg);
		return;
	}

	int attempts = 10;
	do
	{
		DeviceInfo deviceInfo;

		if (find_device_info(enumerator, serial_number, &deviceInfo) != 0)
		{
			Sleep(300);
			continue;
		}
		
		printf("Device with SN %llu found. Name: %s, Address: %s\n", serial_number, deviceInfo.Name, deviceInfo.Address);
		enumerator_delete(enumerator);
		return;
	}
	while (attempts-- > 0);
	
	enumerator_delete(enumerator);
	printf("Device with SN %llu not found.\n", serial_number);
}

int main()
{
	while (true)
	{
		printf("Enter a serial number of a device or \"q\" to exit.\n");

		uint64_t serialNumber;
		if (scanf_s("%llu", &serialNumber, (rsize_t)sizeof(uint64_t)) == 0)
		{
			if (fgetc(stdin) == 'q') 
			{
				while (fgetc(stdin) != '\n') {}
				break;
			}
			else
			{
				while (fgetc(stdin) != '\n'){}
				printf("Invalid command\n");
				continue;
			}
		}
		
		find_by_serial(serialNumber);
	}

	return 0;
}