#include "cscanner.h"
#include "c_eeg_channels.h"
#include "cdevice.h"
#include "sdk_error.h"
#include "stdio.h"
#include "windows.h"

ListenerHandle BatteryListener = NULL;
int BatteryCharge = 0;


ListenerHandle T3ResistanceListener = NULL;
ListenerHandle T4ResistanceListener = NULL;
ListenerHandle O1ResistanceListener = NULL;
ListenerHandle O2ResistanceListener = NULL;

#define RESISTANCE_AVERAGE_COUNT 10

typedef struct _AverageResistance
{
	double Value;
	size_t AverageCount;
} AverageResistance;

AverageResistance T3AverageResistance;
AverageResistance T4AverageResistance;
AverageResistance O1AverageResistance;
AverageResistance O2AverageResistance;

void free_resistance_listeners()
{
	free_listener_handle(T3ResistanceListener);
	free_listener_handle(T4ResistanceListener);
	free_listener_handle(O1ResistanceListener);
	free_listener_handle(O2ResistanceListener);

	T3ResistanceListener = NULL;
	T4ResistanceListener = NULL;
	O1ResistanceListener = NULL;
	O2ResistanceListener = NULL;
}


#define SIGNAL_SAMPLES_COUNT 20

EegDoubleChannel* T3Signal = NULL;
EegDoubleChannel* T4Signal = NULL;
EegDoubleChannel* O1Signal = NULL;
EegDoubleChannel* O2Signal = NULL;


void on_battery_charge_received(Device* device, ChannelInfo channelInfo, IntDataArray batteryData, void* user_data)
{
	if (batteryData.samples_count > 0)
	{
		BatteryCharge = batteryData.data_array[batteryData.samples_count - 1];
	}
}

void print_battery_charge(Device* device)
{
	ChannelInfoArray deviceChannels;
	int resultCode = device_available_channels(device, &deviceChannels);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot get device channels info: %s\n", errorMsg);
		return;
	}

	BatteryCharge = -1;

	for (size_t i = 0; i < deviceChannels.info_count; ++i)
	{
		if (deviceChannels.info_array[i].type == ChannelTypeBattery)
		{
			resultCode = device_subscribe_int_channel_data_received(device, deviceChannels.info_array[i], &on_battery_charge_received, &BatteryListener, NULL);
			if (resultCode != SDK_NO_ERROR)
			{
				char errorMsg[1024];
				sdk_last_error_msg(errorMsg, 1024);
				printf("Cannot subscribe battery channel notifications: %s\n", errorMsg);
				return;
			}
		}
	}
	free_ChannelInfoArray(deviceChannels);

	while (BatteryCharge < 0)
	{
		//waiting for battery charge
	}

	free_listener_handle(BatteryListener);
	BatteryListener = NULL;

	printf("Battery charge: %d\n", BatteryCharge);
}

void append_resistance_value(AverageResistance *resistance, DoubleDataArray resistance_array)
{
	for (size_t i = 0; 
		i < resistance_array.samples_count && resistance->AverageCount < RESISTANCE_AVERAGE_COUNT; 
		++i, ++resistance->AverageCount)
	{
		resistance->Value += resistance_array.data_array[i] / RESISTANCE_AVERAGE_COUNT;
	}
}

void on_resistance_received(Device *device, ChannelInfo channel_info, DoubleDataArray data_array, void* user_data)
{
	if (channel_info.type == ChannelTypeResistance)
	{
		//accumulating RESISTANCE_AVERAGE_COUNT resistance values for each channel
		if (strcmp(channel_info.name, "T3") == 0)
		{
			append_resistance_value(&T3AverageResistance, data_array);
		}
		if (strcmp(channel_info.name, "T4") == 0)
		{
			append_resistance_value(&T4AverageResistance, data_array);
		}
		if (strcmp(channel_info.name, "O1") == 0)
		{
			append_resistance_value(&O1AverageResistance, data_array);
		}
		if (strcmp(channel_info.name, "O2") == 0)
		{
			append_resistance_value(&O2AverageResistance, data_array);
		}
	}

	free_DoubleDataArray(data_array);
}

void check_resistance(Device *device)
{
	printf("Measuring electrodes resistance...\n");
	
	ChannelInfoArray deviceChannels;
	int resultCode = device_available_channels(device, &deviceChannels);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot get device channels info: %s\n", errorMsg);
		return;
	}

	T3AverageResistance.AverageCount = 0;
	T3AverageResistance.Value = 0.0;
	T4AverageResistance.AverageCount = 0;
	T4AverageResistance.Value = 0.0;
	O1AverageResistance.AverageCount = 0;
	O1AverageResistance.Value = 0.0;
	O2AverageResistance.AverageCount = 0;
	O2AverageResistance.Value = 0.0;
	
	for (size_t i = 0; i < deviceChannels.info_count; ++i)
	{
		if (deviceChannels.info_array[i].type == ChannelTypeResistance)
		{
			if (strcmp(deviceChannels.info_array[i].name, "T3") == 0)
			{
				device_subscribe_double_channel_data_received(device, deviceChannels.info_array[i], &on_resistance_received, &T3ResistanceListener, NULL);
			}
			if (strcmp(deviceChannels.info_array[i].name, "T4") == 0)
			{
				device_subscribe_double_channel_data_received(device, deviceChannels.info_array[i], &on_resistance_received, &T4ResistanceListener, NULL);
			}
			if (strcmp(deviceChannels.info_array[i].name, "O1") == 0)
			{
				device_subscribe_double_channel_data_received(device, deviceChannels.info_array[i], &on_resistance_received, &O1ResistanceListener, NULL);
			}
			if (strcmp(deviceChannels.info_array[i].name, "O2") == 0)
			{
				device_subscribe_double_channel_data_received(device, deviceChannels.info_array[i], &on_resistance_received, &O2ResistanceListener, NULL);
			}
		}
	}
	free_ChannelInfoArray(deviceChannels);

	resultCode = device_execute(device, CommandStartResist);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot execute StartResist command: %s\n", errorMsg);
		free_resistance_listeners();
		return;
	}
	
	while (T3AverageResistance.AverageCount < RESISTANCE_AVERAGE_COUNT 
		|| T4AverageResistance.AverageCount < RESISTANCE_AVERAGE_COUNT
		|| O1AverageResistance.AverageCount < RESISTANCE_AVERAGE_COUNT
		|| O2AverageResistance.AverageCount < RESISTANCE_AVERAGE_COUNT)
	{
		//waiting for resistance
	}

	resultCode = device_execute(device, CommandStopResist);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot execute StartResist command: %s\n", errorMsg);
		free_resistance_listeners();
		return;
	}

	free_resistance_listeners();

	//printing resistance in kOhms
	printf("Average resistance T3: %.2fk T4: %.2fk O1: %.2fk O2: %.2fk\n", 
		T3AverageResistance.Value / 1e3, 
		T4AverageResistance.Value / 1e3,
		O1AverageResistance.Value / 1e3,
		O2AverageResistance.Value / 1e3);
}

void print_signal(Device* device)
{
	printf("Receiving signal...\n");

	ChannelInfoArray deviceChannels;
	int resultCode = device_available_channels(device, &deviceChannels);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot get device channels info: %s\n", errorMsg);
		return;
	}

	for (size_t i = 0; i < deviceChannels.info_count; ++i)
	{
		if (deviceChannels.info_array[i].type == ChannelTypeSignal)
		{
			if (strcmp(deviceChannels.info_array[i].name, "T3") == 0)
			{
				T3Signal = create_EegDoubleChannel_info(device, deviceChannels.info_array[i]);
			}
			if (strcmp(deviceChannels.info_array[i].name, "T4") == 0)
			{
				T4Signal = create_EegDoubleChannel_info(device, deviceChannels.info_array[i]);
			}
			if (strcmp(deviceChannels.info_array[i].name, "O1") == 0)
			{
				O1Signal = create_EegDoubleChannel_info(device, deviceChannels.info_array[i]);
			}
			if (strcmp(deviceChannels.info_array[i].name, "O2") == 0)
			{
				O2Signal = create_EegDoubleChannel_info(device, deviceChannels.info_array[i]);
			}
		}
	}
	free_ChannelInfoArray(deviceChannels);


	resultCode = device_execute(device, CommandStartSignal);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot execute StartSignal command: %s\n", errorMsg);
		return;
	}

	size_t t3Length = 0;
	size_t t4Length = 0;
	size_t o1Length = 0;
	size_t o2Length = 0;

	do
	{
		AnyChannel_get_total_length(T3Signal, &t3Length);
		AnyChannel_get_total_length(T4Signal, &t4Length);
		AnyChannel_get_total_length(O1Signal, &o1Length);
		AnyChannel_get_total_length(O2Signal, &o2Length);
	} while (t3Length < SIGNAL_SAMPLES_COUNT
		  || t4Length < SIGNAL_SAMPLES_COUNT
		  || o1Length < SIGNAL_SAMPLES_COUNT
		  || o2Length < SIGNAL_SAMPLES_COUNT);

	resultCode = device_execute(device, CommandStopSignal);
	if (resultCode != SDK_NO_ERROR)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Cannot execute StartResist command: %s\n", errorMsg);
		return;
	}

	double t3SignalBuffer[SIGNAL_SAMPLES_COUNT];
	double t4SignalBuffer[SIGNAL_SAMPLES_COUNT];
	double o1SignalBuffer[SIGNAL_SAMPLES_COUNT];
	double o2SignalBuffer[SIGNAL_SAMPLES_COUNT];

    DoubleChannel_read_data(T3Signal, 0, SIGNAL_SAMPLES_COUNT, t3SignalBuffer, SIGNAL_SAMPLES_COUNT, &t3Length);
    DoubleChannel_read_data(T4Signal, 0, SIGNAL_SAMPLES_COUNT, t4SignalBuffer, SIGNAL_SAMPLES_COUNT, &t4Length);
    DoubleChannel_read_data(O1Signal, 0, SIGNAL_SAMPLES_COUNT, o1SignalBuffer, SIGNAL_SAMPLES_COUNT, &o1Length);
    DoubleChannel_read_data(O2Signal, 0, SIGNAL_SAMPLES_COUNT, o2SignalBuffer, SIGNAL_SAMPLES_COUNT, &o2Length);
	
	printf("%d signal samples are received.\n", SIGNAL_SAMPLES_COUNT);
	printf("     T3:\t     T4:\t     O1:\t     O2:\n");
	for (size_t i = 0; i < SIGNAL_SAMPLES_COUNT; ++i)
	{
		printf("%.2fuV\t%.2fuV\t%.2fuV\t%.2fuV\n", 
			t3SignalBuffer[i]*1e6,
			t4SignalBuffer[i]*1e6,
			o1SignalBuffer[i]*1e6,
			o2SignalBuffer[i]*1e6);
	}

	AnyChannel_delete(T3Signal);
	T3Signal = NULL;
	AnyChannel_delete(T4Signal);
	T4Signal = NULL;
	AnyChannel_delete(O1Signal);
	O1Signal = NULL;
	AnyChannel_delete(O2Signal);
	O2Signal = NULL;
}

Device* find_device(DeviceEnumerator* enumerator, uint64_t serial_number)
{
	DeviceInfoArray deviceInfoArray;
	const int resultCode = enumerator_get_device_list(enumerator, &deviceInfoArray);
	if (resultCode != SDK_NO_ERROR)
	{
		return NULL;
	}
	
	for (size_t i = 0; i < deviceInfoArray.info_count; ++i)
	{
		if (deviceInfoArray.info_array[i].SerialNumber == serial_number)
		{
			const DeviceInfo deviceInfo = deviceInfoArray.info_array[i];
			free_DeviceInfoArray(deviceInfoArray);
			return create_Device(enumerator, deviceInfo);
		}
	}

	free_DeviceInfoArray(deviceInfoArray);
	return NULL;
}

void receive_data(uint64_t serial_number)
{
	DeviceEnumerator* enumerator = create_device_enumerator(DeviceTypeBrainbit);
	if (enumerator == NULL)
	{
		char errorMsg[1024];
		sdk_last_error_msg(errorMsg, 1024);
		printf("Device enumerator is null: %s\n", errorMsg);
		return;
	}

	int attempts = 20;
	do
	{
		Device* device = find_device(enumerator, serial_number);
		if (device == NULL)
		{
			Sleep(300);
			continue;
		}

		const int resultCode = device_connect(device);
		if (resultCode != SDK_NO_ERROR)
		{
			char errorMsg[1024];
			sdk_last_error_msg(errorMsg, 1024);
			printf("Cannot connect to device: %s\n", errorMsg);
			device_delete(device);
			continue;
		}

		print_battery_charge(device);
		check_resistance(device);		
		print_signal(device);
		
		device_disconnect(device);
		device_delete(device);
		enumerator_delete(enumerator);
		return;
	}
	while (attempts-- > 0);
	
	enumerator_delete(enumerator);
	printf("Device with SN %llu not found.\n", serial_number);
}

int main()
{
	while (true)
	{
		printf("Enter a serial number of a device or \"q\" to exit.\n");

		uint64_t serialNumber;
		if (scanf_s("%llu", &serialNumber, (rsize_t)sizeof(uint64_t)) == 0)
		{
			if (fgetc(stdin) == 'q') 
			{
				while (fgetc(stdin) != '\n') {}
				break;
			}
			else
			{
				while (fgetc(stdin) != '\n'){}
				printf("Invalid command\n");
				continue;
			}
		}
		
		receive_data(serialNumber);
	}

	return 0;
}