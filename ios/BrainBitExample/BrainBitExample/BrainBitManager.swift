//
//  BrainBitManager.swift
//  BrainBitExample
//
//  Created by Vladimir on 19.01.2021.
//
import UIKit
import neurosdk

protocol StateDelegate:class {
    func batteryValueDidChange(newValue:Int)
    func connectionStateDidChange(newState: NTState)
}

protocol EegDataDelegate:class {
    func dataChanged(channelName: String, newValue: Double)
}

protocol ResistanceDataDelegate {
    func dataChanged(channelName: String, newValue: Double)
}

protocol EegIndexesDelegate {
    func dataChanged(alpha: Double, beta: Double, theta: Double, delta: Double)
}

protocol MeditationDelegate {
    func dataChanged(level: Int?, levelProgress: Double?)
}

protocol EmotionsDelegate {
    func calibrationChanged(progress: Double)
    func emotionChanged(relaxation: Double, concentration: Double, meditation: Double)
}

class BrainBitManager: NSObject {

    static let shared = BrainBitManager()
    
    private var scanner: NTDeviceEnumerator?
    private var device: NTDevice?
    private var deviceInfo: NTDeviceInfo?

    // channels
    private var batteryChannel: NTBatteryChannel? = nil
    private var eegChannels: [String: NTEegChannel?] = [:]
    private var resistChannels: [String: NTResistanceChannel?] = [:]
    /*private var bipolarChannels: [String: NTBipolarDoubleChannel?] = [:]
    private var spectrumChannels: [String: NTSpectrumChannel?] = [:]
    private var spectrumPowerChannels: [String: NTSpectrumPowerDoubleChannel?] = [:]
    */
    private var indexChannel: NTEegIndexChannel? = nil
    private var meditationAnalyzerIndexChannel: NTEegIndexChannel? = nil
    //private var emotionsIndexChannel: NTEegIndexChannel? = nil
    
    //private var emotionsChannel: NTEmotionalStateChannel? = nil
    //private var artifactChannel: NTEegArtifactChannel? = nil
    private var meditationAnalyzerChannel: NTMeditationAnalyzerChannel? = nil
    private var emotionsAnalyzer: NTEmotionsAnalyzer? = nil
    private var channelsInitialized: Bool = false
    
    public var eegDelegates: [EegDataDelegate] = []
    public var resistanceDelegates: [ResistanceDataDelegate] = []
    public var indexesDelegate: EegIndexesDelegate?
    public var meditationDelegate: MeditationDelegate?
    public var emotionsDelegate: EmotionsDelegate?
    
    weak var stateDelegate: StateDelegate?
    
    public var batteryLevel: Int = 0 {
        didSet {
            stateDelegate?.batteryValueDidChange(newValue: batteryLevel)
        }
    }
    
    public var connectionState: NTState = .disconnected {
        didSet {
            stateDelegate?.connectionStateDidChange(newState: connectionState)
        }
    }
    
    private override init() {
        super.init()
    }
    
    func startSearching(deviceLabel: UILabel ) {
        scanner = NTDeviceEnumerator(deviceType: .TypeBrainbit)
        
        if let scanner = scanner {
            scanner.subscribeFoundDevice { deviceInfo in
                self.device = NTDevice(enumerator: scanner, deviceInfo: deviceInfo)
                DispatchQueue.main.async {
                    deviceLabel.text = deviceInfo.name + " " + String(deviceInfo.serialNumber)
                }
                self.deviceInfo = deviceInfo
                
                if let d = self.device {
                    d.subscribeParameterChanged(subscriber: { param in
                        switch param {
                        case .state:
                            let state = d.state(error: nil)
                            self.connectionState = state
                            break

                            
                        default:
                            break
                        }
                    })
                }
                
                self.device?.connect()
                
                self.initChannels()
            }
        }
    }
    
    func initChannels() {
        if channelsInitialized {
            return
        }
        print("Initialization channels")
        initBattery()
        initResistanceChannels()
        initEegChannels()
        initIndexChannel()
        initMeditationAnalyzerChannel()
        initEmotionsAnalyzer()
        /*initBipolarChannels()
        initSpectrumChannel(.hamming)
        initSpectrumPowerChannel()
        initArtifactChannel()
        
//        initEmotionsChannel()
        */
        channelsInitialized = true
    }
    
    private func initBattery() {
        guard let device = self.device else { return }
        self.batteryChannel = NTBatteryChannel(device: device)

        guard let channel = batteryChannel else { return }
        channel.subscribeLengthChanged(subscribe: { length in
            guard let level = channel.readData(offset: length - 1, length: 1).first else { return }
            self.batteryLevel = level.intValue
            
        })
    }
    
    private func initResistanceChannels() {
        guard let device = self.device else { return }
        do {
            try NTDeviceTraits.hasChannels(withType: device, channelType: .resistance)
            NTDeviceTraits.getChannelInfoArray(withType: device, channelType: .resistance, error: nil).forEach {
                self.resistChannels[$0.name] = NTResistanceChannel(device: device, channelInfo: $0)
            }
            print("initResistanceChannels succeeded")
        }
        catch let err as NSError{
            print("initResistanceChannels failed")
        }
        
    }
    
    func readResistanceData() {
        
        self.device?.execute(command: .startResist)
        
        self.resistChannels.forEach{ (channelName, channel) in
            channel?.subscribeLengthChanged(subscribe: { (length) in
                    let newdata = channel?.readData(offset: length-1, length: 1).first
                    DispatchQueue.main.async {
                        self.resistanceDelegates.forEach{ delegate in
                            delegate.dataChanged(channelName: channelName, newValue: newdata as! Double)
                    }
                }
            })
        }
    }
    
    func stopReadingResistanceData() {
        self.resistChannels.forEach{ (_, channel) in
            channel?.subscribeLengthChanged(subscribe: nil)
        }
        self.resistanceDelegates.removeAll()
        
        self.device?.execute(command: .stopResist)
    }
    
    private func initEegChannels() {
        guard let device = self.device else { return }
        do {
            try NTDeviceTraits.hasChannels(withType: device, channelType: .signal)
            NTDeviceTraits.getChannelInfoArray(withType: device, channelType: .signal, error: nil).forEach {
                eegChannels[$0.name] = NTEegChannel(device: device, channelInfo: $0)
            }
            print("initEegChannels succeeded")
            
        }
        catch let err as NSError{
            print("initEegChannels failed")
        }
    }
    
    func readEegData() {
        
        self.device?.execute(command: .startSignal)
        
        self.eegChannels.forEach { (channelName, channel) in
            channel?.subscribeLengthChanged(subscribe: { (length) in
                   let newdata = channel?.readData(offset: length-1, length: 1).first
                    DispatchQueue.main.async {
                        self.eegDelegates.forEach { delegate in
                            delegate.dataChanged(channelName: channelName, newValue: newdata as! Double)
                        }
                    }
                })
        }
    }
    
    func stopReadingEegData() {
        self.eegChannels.forEach{ (_, channel) in
            channel?.subscribeLengthChanged(subscribe: nil)
        }
        self.eegDelegates.removeAll()
        
        self.device?.execute(command: .stopSignal)
    }
    
    private func initIndexChannel() {
        guard let t3 = eegChannels["T3"], let t4 = eegChannels["T4"], let o1 = eegChannels["O1"], let o2 = eegChannels["O2"] else {
            print("eeg channels is not avaliable")
            return
        }
        indexChannel = NTEegIndexChannel(t3: t3!, t4: t4!, o1: o1!, o2: o2!)
        
        meditationAnalyzerIndexChannel = NTEegIndexChannel(t3: t3!, t4: t4!, o1: o1!, o2: o2!)
        
        print("initIndexChannel succeeded")
    }
    
    func readIndexesData() {
        
        self.device?.execute(command: .startSignal)

        guard let channel = self.indexChannel else {
            print("index channel is not ready")
            return
        }
        channel.subscribeLengthChanged(subscribe: { length in
            if channel.totalLength < 1 {
                print("channel is not ready yet")
                return
            }
            if let current = channel.readData(offset: length - 1, length: 1).first {
                DispatchQueue.main.async {
                    self.indexesDelegate?.dataChanged(alpha: current.alphaRate, beta: current.betaRate, theta: current.thetaRate, delta: current.deltaRate)
                }
                
            }
        })
    }
    
    func stopReadingIndexesData() {
        guard let channel = self.indexChannel else { return }
        channel.subscribeLengthChanged(subscribe: nil)
        self.indexesDelegate = nil
        
        self.device?.execute(command: .stopSignal)
    }
    
    public func initMeditationAnalyzerChannel() {
        guard let meditationIndexChannel = self.meditationAnalyzerIndexChannel else {
            print("index channel is not ready")
            return
        }
        
        deinitMeditationAnalyzerChannel()
        meditationIndexChannel.setWeightCoefficientsWithAlpha(1, beta: 1, delta: 0, theta: 1)
        // for meditation debug purpose you may uncomment this line to see meditation level progress
        //meditationIndexChannel.setWeightCoefficientsWithAlpha(1, beta: 0, delta: 0, theta: 0)
        meditationAnalyzerChannel = NTMeditationAnalyzerChannel(indexChannel: meditationIndexChannel)
    }
    
    func deinitMeditationAnalyzerChannel() {
        unsubscribeMeditationAnalyzerChannel()
        self.meditationAnalyzerIndexChannel?.setWeightCoefficientsWithAlpha(1, beta: 1, delta: 1, theta: 1)
        
        self.meditationAnalyzerChannel = nil
    }
    
    func unsubscribeMeditationAnalyzerChannel() {
        guard let meditationAnalyzerChannel = self.meditationAnalyzerChannel else {
            return
        }
        meditationAnalyzerChannel.subscribeLevelChanged(subscribe: nil)
        meditationAnalyzerChannel.subscribeLevelProgressChanged(subscribe: nil )
        print("unsubscribeMeditationAnalyzerChannel succeeded")
    }
    
    func readMeditationChannel() {

        guard let meditation = self.meditationAnalyzerChannel else {
            print("meditation channel is not ready")
            return
        }
        meditation.subscribeLevelChanged { (newlevel) in
            DispatchQueue.main.async {
                self.meditationDelegate?.dataChanged(level: newlevel, levelProgress: nil)
            }
        }
        
        meditation.subscribeLevelProgressChanged { (newlevelprogess) in
            DispatchQueue.main.async {
                self.meditationDelegate?.dataChanged(level: nil, levelProgress: newlevelprogess)
            }
        }
        print("subscribeMeditationChannel succeeded")
        
        self.device?.execute(command: .startSignal)
    }
    
    func stopReadingMeditationChannel() {
        deinitMeditationAnalyzerChannel()
        
        self.meditationDelegate = nil
        
        self.device?.execute(command: .stopSignal)
    }
    
    private func initEmotionsAnalyzer() {
        guard let device = self.device else { return }
        
        self.emotionsAnalyzer = NTEmotionsAnalyzer(device: device)
        
        
    }
    
    func analyzeEmotions() {
        self.device?.execute(command: .startSignal)
        
        let channelWeights : NTIndexValues = NTIndexValues(alpha: 1.0, beta: 1.0, delta: 0.0, theta: 0.0)
        self.emotionsAnalyzer?.setWeights(channelWeights, error: nil)
        
        self.emotionsAnalyzer?.calibrate(nil)
        
        if let analyzer = self.emotionsAnalyzer {
            analyzer.subscribeAnalyzerStateChanged(subscribe: {(analyzerState, calibrationStatus) in
                DispatchQueue.main.async {
                    self.emotionsDelegate?.calibrationChanged(progress: Double(calibrationStatus.progress))
                }
                if analyzerState == .working { // calibration is over
                    DispatchQueue.main.async {
                        self.emotionsDelegate?.calibrationChanged(progress: 1.0)
                    }
                    analyzer.subscribeEmotionalStateChanged(subscribe: {(emotionsSample) in
                        DispatchQueue.main.async {
                            self.emotionsDelegate?.emotionChanged(relaxation: Double(emotionsSample.relaxationRate), concentration: Double(emotionsSample.concentrationRate), meditation: Double(emotionsSample.meditationProgress))
                        }
                    })
                }
            })
        }
    }
    
    func stopAnalyzeEmotions() {
        let channelWeights : NTIndexValues = NTIndexValues(alpha: 1.0, beta: 1.0, delta: 1.0, theta: 1.0)
        self.emotionsAnalyzer?.setWeights(channelWeights, error: nil)
        
        self.emotionsAnalyzer?.subscribeEmotionalStateChanged(subscribe: nil)
        self.emotionsAnalyzer?.subscribeAnalyzerStateChanged(subscribe: nil)
        
        self.emotionsDelegate = nil
        
        self.device?.execute(command: .stopSignal)
    }
    
    func getDeviceInfo() -> String {
        var result : String = ""
        
        if let device = self.device {
            
            if let deviceInfo = self.deviceInfo {
                result = "Name: " + deviceInfo.name + " \n"
                result = result + "Serial Number: " + String(deviceInfo.serialNumber) + " \n"
                result = result + "Address: " + deviceInfo.address + " \n"
            }
            
            result = result + "Battery Level: " + String(self.batteryLevel) + "% \n"
            /*if let parameters = device.parameters {
                
            }*/
            
            //result = result + batteryChannel.
            
            
            // channels
            /*if let channels = device.channels {
                for i in 0..<channels.count {
                    result = result + channels[i].name + " "
                    result = result + String(channels[i].index) + " "
                }
            }*/
        }
        return result
    }
}
