//
//  MeditationViewController.swift
//  BrainBitExample
//
//  Created by Vladimir on 10.02.2021.
//

import UIKit

class MeditationViewController: UIViewController, MeditationDelegate {

    @IBOutlet weak var LevelTitle: UILabel!
    @IBOutlet weak var LevelProgressTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        BrainBitManager.shared.meditationDelegate = self
        BrainBitManager.shared.readMeditationChannel()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        BrainBitManager.shared.stopReadingMeditationChannel()
    }
    
    func dataChanged(level: Int?, levelProgress: Double?) {
        if level == nil {
            LevelProgressTitle.text = "Level Progress: " + String(format: "%.0f", levelProgress! * 100 )
        }
        if levelProgress == nil {
            LevelTitle.text = "Level: " + String(level!)
        }
    }

}
