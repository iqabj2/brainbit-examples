//
//  ViewController.swift
//  BrainBitExample
//
//  Created by Vladimir on 15.01.2021.
//

import UIKit
import neurosdk

class ViewController: UIViewController, StateDelegate {

    @IBOutlet weak var DeviceSearchButton: UIButton!
    
    @IBOutlet weak var DeviceInfoBtn: UIButton!
    
    @IBOutlet weak var EegBtn: UIButton!
    
    @IBOutlet weak var BatteryValueToolbarItem: UIBarButtonItem!
    
    @IBOutlet weak var ConnectionStateToolbarItem: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.barTintColor = UIColor.systemGray6 // fixed unknown shadow appearence
        BrainBitManager.shared.stateDelegate = self
    }
    
    func batteryValueDidChange(newValue: Int) {
        BatteryValueToolbarItem.title = String(newValue) + "%"
    }
    
    func connectionStateDidChange(newState: NTState) {
        switch newState {
        case .connected:
            ConnectionStateToolbarItem.title = "Connected"
            break
        case .disconnected:
            ConnectionStateToolbarItem.title = "Disconnected"
            BatteryValueToolbarItem.title = "-"
            break
        default:
            break
        }
    }
    
    //-------------------------------------------------------------------------

    @IBAction func DeviceSearchAction(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let searchController = storyboard.instantiateViewController(identifier: "DeviceSearchViewController")
        navigationController?.pushViewController(searchController, animated: true)
    }
    
    @IBAction func DeviceInfoBtnAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let infoController = storyboard.instantiateViewController(identifier: "DeviceInfoViewController")
        navigationController?.pushViewController(infoController, animated: true)
    }
    
    @IBAction func ResistanceBtnAction(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let resistanceController = storyboard.instantiateViewController(identifier: "ResistanceViewController")
        navigationController?.pushViewController(resistanceController, animated: true)
        
        
    }
    
    @IBAction func EegBtnAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let signalController = storyboard.instantiateViewController(identifier: "SignalViewController")
        navigationController?.pushViewController(signalController, animated: true)
    }
    

    @IBAction func EegIndexesBtnAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let indexesController = storyboard.instantiateViewController(identifier: "EegIndexesViewController")
        navigationController?.pushViewController(indexesController, animated: true)
    }
    
    @IBAction func MeditationBtnAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let meditationControlller = storyboard.instantiateViewController(identifier: "MeditationViewController")
        navigationController?.pushViewController(meditationControlller, animated: true)
    }
    @IBAction func EmotionsAnalyzerBtnAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let emotionsController = storyboard.instantiateViewController(identifier: "EmotionsViewController")
        navigationController?.pushViewController(emotionsController, animated: true)
    }
}
