//
//  DeviceSearchViewController.swift
//  BrainBitExample
//
//  Created by Vladimir on 19.01.2021.
//

import UIKit

class DeviceSearchViewController: UIViewController {

    @IBOutlet weak var startSearchBtn: UIButton!
    @IBOutlet weak var deviceLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func startSearchBtnAction(_ sender: Any) {
        BrainBitManager.shared.startSearching(deviceLabel: deviceLabel)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
