//
//  ResistanceViewController.swift
//  BrainBitExample
//
//  Created by Vladimir on 04.02.2021.
//

import UIKit

class ResistanceViewController: UIViewController, ResistanceDataDelegate {
    @IBOutlet weak var T3Title: UILabel!
    @IBOutlet weak var T4Title: UILabel!
    @IBOutlet weak var O1Title: UILabel!
    @IBOutlet weak var O2Title: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        BrainBitManager.shared.resistanceDelegates.append(self)
        BrainBitManager.shared.readResistanceData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        BrainBitManager.shared.stopReadingResistanceData()
    }
    
    func dataChanged(channelName: String, newValue: Double) {
        switch channelName {
        case "T3":
            T3Title.text = "T3: " + String(format:"%.2f", newValue / 1000) + " kOm"
            break
        case "T4":
            T4Title.text = "T4: " + String(format:"%.2f", newValue / 1000) + " kOm"
            break
        case "O1":
            O1Title.text = "O1: " + String(format:"%.2f", newValue / 1000) + " kOm"
            break
        case "O2":
            O2Title.text = "O2: " + String(format:"%.2f", newValue / 1000) + " kOm"
            break
        default:
            break
        }
    }

}
