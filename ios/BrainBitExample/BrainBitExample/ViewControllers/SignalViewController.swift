//
//  SignalViewController.swift
//  BrainBitExample
//
//  Created by Vladimir on 29.01.2021.
//

import UIKit

class SignalViewController: UIViewController {


    @IBOutlet weak var T3EegView: MyGraphView!
    @IBOutlet weak var T4EegView: MyGraphView!
    @IBOutlet weak var O1EegView: MyGraphView!
    @IBOutlet weak var O2EegView: MyGraphView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        T3EegView.setChannelName(name: "T3")
        T3EegView.initGraph()
        BrainBitManager.shared.eegDelegates.append(T3EegView)
        
        T4EegView.setChannelName(name: "T4")
        T4EegView.initGraph()
        BrainBitManager.shared.eegDelegates.append(T4EegView)
        
        O1EegView.setChannelName(name: "O1")
        O1EegView.initGraph()
        BrainBitManager.shared.eegDelegates.append(O1EegView)
        
        O2EegView.setChannelName(name: "O2")
        O2EegView.initGraph()
        BrainBitManager.shared.eegDelegates.append(O2EegView)
        
        BrainBitManager.shared.readEegData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        BrainBitManager.shared.stopReadingEegData()
    }
    
}

