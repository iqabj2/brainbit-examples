//
//  EmotionsViewController.swift
//  BrainBitExample
//
//  Created by Vladimir on 15.02.2021.
//

import UIKit

class EmotionsViewController: UIViewController, EmotionsDelegate {

    @IBOutlet weak var CalibrationProgressTitle: UILabel!
    @IBOutlet weak var RelaxationTitle: UILabel!
    @IBOutlet weak var ConcentrationTitle: UILabel!
    @IBOutlet weak var MeditationTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        BrainBitManager.shared.emotionsDelegate = self
        BrainBitManager.shared.analyzeEmotions()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        BrainBitManager.shared.stopAnalyzeEmotions()
    }
    
    func calibrationChanged(progress: Double) {
        CalibrationProgressTitle.text = "Calibration Progress: " + String(format: "%.2f", progress * 100  ) + "%"
    }
    
    func emotionChanged(relaxation: Double, concentration: Double, meditation: Double) {
        RelaxationTitle.text = "Relaxation Rate: " + String(format: "%.4f",relaxation)
        ConcentrationTitle.text = "Concentration Rate: " + String(format: "%.4f",concentration)
        MeditationTitle.text = "Meditation Progress: " + String(format: "%.4f",meditation)
    }

}
