//
//  EegIndexesViewController.swift
//  BrainBitExample
//
//  Created by Vladimir on 08.02.2021.
//

import UIKit

class EegIndexesViewController: UIViewController, EegIndexesDelegate {

    @IBOutlet weak var AlphaTitle: UILabel!
    @IBOutlet weak var BetaTitle: UILabel!
    @IBOutlet weak var ThetaTitle: UILabel!
    @IBOutlet weak var DeltaTitle: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        BrainBitManager.shared.indexesDelegate = self
        BrainBitManager.shared.readIndexesData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        BrainBitManager.shared.stopReadingIndexesData()
    }
    
    func dataChanged(alpha: Double, beta: Double, theta: Double, delta: Double) {
        AlphaTitle.text = "Alpha: " + String(format:"%.4f", alpha)
        BetaTitle.text = "Beta: " + String(format:"%.4f", beta)
        ThetaTitle.text = "Theta: " + String(format:"%.4f", theta)
        DeltaTitle.text = "Delta: " + String(format:"%.4f", delta)
    }
}
