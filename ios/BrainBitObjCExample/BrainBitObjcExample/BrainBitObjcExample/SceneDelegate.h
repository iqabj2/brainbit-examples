//
//  SceneDelegate.h
//  BrainBitObjcExample
//
//  Created by Aseatari on 19.01.2022.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

