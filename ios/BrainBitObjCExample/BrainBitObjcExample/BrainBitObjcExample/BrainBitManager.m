//
//  BrainBitManager.m
//  BrainBitObjcExample
//
//  Created by Aseatari on 20.01.2022.
//

#import <Foundation/Foundation.h>
#import "BrainBitManager.h"

@interface BrainBitManager ()

- (void) createEnumerator;
- (void) closeEnumerator;

- (void) initBattery;
- (void) destroyBattery;


@end

@implementation BrainBitManager

+ (id) sharedInstance{
    static BrainBitManager* brainBitManagerInstance = nil;
      static dispatch_once_t onceToken;
      dispatch_once( &onceToken, ^{
          brainBitManagerInstance = [ [ self alloc ] init ];
      } );
      return brainBitManagerInstance;
}

- (void) startSearch:(void(^)(NTDeviceInfo*))searchResult {
    [self destroyBattery];
    [self disconnect];
    
    [self createEnumerator];
    
    [_deviceEnumerator subscribeFoundDeviceWithSubscriber:searchResult];
}

- (void) stopSearch{
    [self closeEnumerator];
}

- (void) connect: (NTDeviceInfo*)devInfo{
    _device = [[NTDevice alloc] initWithEnumerator:_deviceEnumerator deviceInfo:devInfo];
    [_device subscribeParameterChangedWithSubscriber:^(NTParameter param){
        if(param == NTParameterState){
            self->_connectionState = [self->_device readState:nil];
            [self->_deviceStateDelegate onStateChange:self->_connectionState];
            if(self->_connectionState == NTStateConnected){
                [self initBattery];
            }
            else {
                [self destroyBattery];
            }
        }
        
    }];
    [_device connect];
    
}

- (void) disconnect{
    _connectionState = NTStateDisconnected;
    [_device subscribeParameterChangedWithSubscriber:nil];
    [_device disconnect];
    _device = nil;
}

- (void) initBattery{
    _batteryChannel = [[NTBatteryChannel alloc] initWithDevice:_device];
    [_batteryChannel subscribeLengthChangedWithSubscribe:^(NSUInteger length) {
        NSUInteger totalLength = [self->_batteryChannel totalLength];
        NSNumber *power = [self->_batteryChannel readDataWithOffset:totalLength - 1 length:1][0];
        [self->_devicePowerDelegate onPowerChange:power];
    }];
}

- (void) destroyBattery{
    [_batteryChannel subscribeLengthChangedWithSubscribe:nil];
    _batteryChannel = nil;
}

- (NSString*) getInfo{
    NSMutableString* info = [[NSMutableString alloc]init];
    [info appendString:@"*Common params*\n"];
    [info appendFormat:@"Name: %@\n", [_device readName:nil]];
    [info appendFormat:@"Address: %@\n", [_device readAddress:nil]];
    [info appendFormat:@"Serial number: %@\n", [_device readSerialNumber:nil]];
    
    NTFirmwareVersion* fv = [_device readFirmwareVersion:nil];
    [info appendFormat:@"Version: %u.%u\n", fv.version, fv.build];
    
    NTFirmwareMode mode = [_device readFirmwareMode:nil];
    switch(mode) {
        case NTFirmwareModeApplication:
            [info appendString:@"Mode: Application\n"];
            break;
        case NTFirmwareModeBootloader:
            [info appendString:@"Mode: Bootloader\n"];
            break;
    }
    
    [info appendString:@"*Supperted params*\n"];
    for (NTParameterInfo* param in _device.parameters){
        switch(param.parameter) {
            case NTParameterName:
                [info appendString:@"Name: Name "];
                break;
            case NTParameterState:
                [info appendString:@"Name: State "];
                break;
            case NTParameterAddress:
                [info appendString:@"Name: Address "];
                break;
            case NTParameterSerialNumber:
                [info appendString:@"Name: SerialNumber "];
                break;
            case NTParameterHardwareFilterState:
                [info appendString:@"Name: HardwareFilterState "];
                break;
            case NTParameterFirmwareMode:
                [info appendString:@"Name: FirmwareMode "];
                break;
            case NTParameterSamplingFrequency:
                [info appendString:@"Name: SamplingFrequency "];
                break;
            case NTParameterGain:
                [info appendString:@"Name: Gain "];
                break;
            case NTParameterOffset:
                [info appendString:@"Name: Offset "];
                break;
            case NTParameterExternalSwitchState:
                [info appendString:@"Name: ExternalSwitchState "];
                break;
            case NTParameterADCInputState:
                [info appendString:@"Name: ADCInputState "];
                break;
            case NTParameterAccelerometerSens:
                [info appendString:@"Name: AccelerometerSens "];
                break;
            case NTParameterGyroscopeSens:
                [info appendString:@"Name: GyroscopeSens "];
                break;
            case NTParameterStimulatorAndMAState:
                [info appendString:@"Name: StimulatorAndMAState "];
                break;
            case NTParameterStimulatorParamPack:
                [info appendString:@"Name: StimulatorParamPack "];
                break;
            case NTParameterMotionAssistantParamPack:
                [info appendString:@"Name: MotionAssistantParamPack "];
                break;
            case NTParameterFirmwareVersion:
                [info appendString:@"Name: FirmwareVersion "];
                break;
            case NTParameterMEMSCalibrationStatus:
                [info appendString:@"Name: MEMSCalibrationStatus "];
                break;
            case NTParameterNone:
                [info appendString:@"Name: None "];
                break;
        }
        
        switch (param.access) {
            case NTParamAccessRead:
                [info appendString:@"Access: Read\n"];
                break;
            case NTParamAccessReadWrite:
                [info appendString:@"Access: ReadWrite\n"];
                break;
            case NTParamAccessReadNotify:
                [info appendString:@"Access: ReadNotify\n"];
                break;
            case NTParamAccessNone:
                [info appendString:@"Access: None\n"];
                break;
        }
    }
    
    [info appendString:@"*Supported device channels*\n"];
    for (NTChannelInfo* channel in _device.channels) {
        [info appendFormat:@"Name: %@ ", channel.name];
        switch (channel.type) {
            case NTChannelTypeSignal:
                [info appendString:@"Type: Signal "];
                break;
            case NTChannelTypeBattery:
                [info appendString:@"Type: Battery "];
                break;
            case NTChannelTypeResistance:
                [info appendString:@"Type: Resistance "];
                break;
            default:
                [info appendString:@"Type: Undefined "];
                break;
        }
        [info appendFormat:@"Index: %ld\n", (long)channel.index];
    }
    
    [info appendString:@"*Supported commands*\n"];
    for(NSNumber* command in _device.commands){
        if(command == [NSNumber numberWithInteger:NTCommandStartSignal] ){ [info appendString:@"StartSignal\n"]; }
        if(command == [NSNumber numberWithInteger:NTCommandStopSignal] ){ [info appendString:@"StopSignal\n"]; }
        if(command == [NSNumber numberWithInteger:NTCommandStartResist] ){ [info appendString:@"StartResist\n"]; }
        if(command == [NSNumber numberWithInteger:NTCommandStopResist] ){ [info appendString:@"StopResist\n"]; }
    }
        
    return info;
}

- (void) initResistance{
    if([NTDeviceTraits HasChannelsWithType:_device channelType:NTChannelTypeResistance error:nil]){
        
        _resistanceChannels = [NSMutableDictionary new];
        
        NSArray<NTChannelInfo*>* channels = [NTDeviceTraits GetChannelInfoArrayWithType:_device channelType:NTChannelTypeResistance error:nil];
        for (NTChannelInfo* info in channels) {
            NTResistanceChannel* channel = [[NTResistanceChannel alloc] initWithDevice:_device channelInfo:info];
            [channel subscribeLengthChangedWithSubscribe:^(NSUInteger length){
                
                NSString* channelName = channel.info.name;
                NTResistanceChannel* channel = self->_resistanceChannels[channelName];
                NSUInteger totalLength = channel.totalLength;
                NSNumber *resist = [channel readDataWithOffset:totalLength - 1 length:1][0];
                [self->_resistDataDelegate onRecieveResist:resist name:channelName];
                
            }];
            [_resistanceChannels setObject:channel forKey:channel.info.name];
        }
    }
}

- (void) destroyResistance{
    for (NTResistanceChannel* channel in _resistanceChannels.allValues) {
        [channel subscribeLengthChangedWithSubscribe:nil];
    }
    [_resistanceChannels removeAllObjects];
}

- (void) startResist{
    [_device executeWithCommand:NTCommandStartResist];
}

- (void) stopResist{
    [_device executeWithCommand:NTCommandStopResist];
}

- (void) initEEG{
    if([NTDeviceTraits HasChannelsWithType:_device channelType:NTChannelTypeSignal error:nil]){
        
        _eegChannels = [NSMutableDictionary new];
        _eegChannelsOffsets = [NSMutableDictionary new];
        
        NSArray<NTChannelInfo*>* channels = [NTDeviceTraits GetChannelInfoArrayWithType:_device channelType:NTChannelTypeSignal error:nil];
        for (NTChannelInfo* info in channels) {
            NTEegChannel* channel = [[NTEegChannel alloc] initWithDevice:_device channelInfo:info];
            [channel subscribeLengthChangedWithSubscribe:^(NSUInteger length){
                
                NSString* channelName = channel.info.name;
                NTEegChannel* channel = self->_eegChannels[channelName];
                NSUInteger channelOffset = [self->_eegChannelsOffsets[channelName] integerValue];
                NSUInteger totalLength = channel.totalLength;
                NSUInteger readLength = totalLength - channelOffset;
                if(readLength < 1) return;
                NSArray* eegSamples = [channel readDataWithOffset:channelOffset length:readLength];
                self->_eegChannelsOffsets[channel.info.name] = [NSNumber numberWithDouble:(channelOffset + readLength)];
                
                [self->_eegDataDelegate onRecieveSignal:eegSamples name:channelName];
                
            }];
            [_eegChannels setObject:channel forKey:channel.info.name];
        }
    }
}

- (void) destroyEEG{
    for (NTEegChannel* channel in _eegChannels.allValues) {
        [channel subscribeLengthChangedWithSubscribe:nil];
    }
    [_eegChannels removeAllObjects];
}

- (void) startSignal{
    [_device executeWithCommand:NTCommandStartSignal];
}

- (void) stopSignal{
    [_device executeWithCommand:NTCommandStopSignal];
}


- (void) createEnumerator{
    [self closeEnumerator];
    _deviceEnumerator = [[NTDeviceEnumerator alloc] initWithDeviceType:TypeBrainbitAny];
}
 
- (void) closeEnumerator{
    [_deviceEnumerator subscribeFoundDeviceWithSubscriber:nil];
    _deviceEnumerator = nil;
}

@end
