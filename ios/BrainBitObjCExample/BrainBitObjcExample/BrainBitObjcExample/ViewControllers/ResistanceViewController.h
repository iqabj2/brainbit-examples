//
//  ResistanceViewController.h
//  BrainBitObjcExample
//
//  Created by Aseatari on 20.01.2022.
//

#ifndef ResistanceViewController_h
#define ResistanceViewController_h

#import <UIKit/UIKit.h>
#import "BrainBitManager.h"

@interface ResistanceViewController : UIViewController<ResistData>


@end

#endif /* ResistanceViewController_h */
