//
//  DeviceSearchViewController.h
//  BrainBitObjcExample
//
//  Created by Aseatari on 20.01.2022.
//

#ifndef DeviceSearchViewController_h
#define DeviceSearchViewController_h

#import <UIKit/UIKit.h>
#import "BrainBitManager.h"

@interface DeviceSearchViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, DeviceState>


@end

#endif /* DeviceSearchViewController_h */
