//
//  EEGViewController.m
//  BrainBitObjcExample
//
//  Created by Aseatari on 20.01.2022.
//

#import <Foundation/Foundation.h>
#import "EEGViewController.h"
#import "SignalGraphView.h"

@interface EEGViewController ()
@property (weak, nonatomic) IBOutlet SignalGraphView *O1Graph;
@property (weak, nonatomic) IBOutlet SignalGraphView *O2Graph;
@property (weak, nonatomic) IBOutlet SignalGraphView *T3Graph;
@property (weak, nonatomic) IBOutlet SignalGraphView *T4Graph;


@end

@implementation EEGViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_O1Graph initGraph:@"O1"];
    [_O2Graph initGraph:@"O2"];
    [_T3Graph initGraph:@"T3"];
    [_T4Graph initGraph:@"T4"];
    
}

-(void)viewDidAppear:(BOOL)animated{
    BrainBitManager* manager = [BrainBitManager sharedInstance];
    manager.eegDataDelegate = self;
    [manager initEEG];
    [manager startSignal];
}

- (void) onRecieveSignal:(NSArray *)data name:(NSString *)name{
    NSMutableArray* array = [[NSMutableArray alloc] initWithArray:data];
    for (int i = 0; i < [array count]; i++) {
        NSNumber *sample = [[NSNumber alloc] initWithDouble:[[array objectAtIndex:i] doubleValue] * 1e6];
        [array removeObjectAtIndex:i];
        [array insertObject:sample atIndex:i];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        if([name isEqual: @"O1"]){
            [self->_O1Graph dataChanged:name newValues: array];
        }
        if([name isEqual: @"O2"]){
            [self->_O2Graph dataChanged:name newValues: array];
        }
        if([name isEqual: @"T3"]){
            [self->_T3Graph dataChanged:name newValues: array];
        }
        if([name isEqual: @"T4"]){
            [self->_T4Graph dataChanged:name newValues: array];
        }
    });
    
}

- (void)viewDidDisappear:(BOOL)animated{
    BrainBitManager* manager = [BrainBitManager sharedInstance];
    manager.eegDataDelegate = nil;
    [manager destroyEEG];
    [manager stopSignal];
}

@end
