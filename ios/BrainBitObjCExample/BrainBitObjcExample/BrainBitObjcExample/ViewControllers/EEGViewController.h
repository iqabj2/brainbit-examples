//
//  EEGViewController.h
//  BrainBitObjcExample
//
//  Created by Aseatari on 20.01.2022.
//

#ifndef EEGViewController_h
#define EEGViewController_h

#import "BrainBitManager.h"

@interface EEGViewController : UIViewController<EEGData>


@end


#endif /* EEGViewController_h */
