//
//  DeviceSearchViewController.m
//  BrainBitObjcExample
//
//  Created by Aseatari on 20.01.2022.
//

#import <Foundation/Foundation.h>
#import "DeviceSearchViewController.h"

@interface DeviceSearchViewController (){
    BOOL searching;
    NSMutableArray* devices;
}
@property (weak, nonatomic) IBOutlet UITableView *availableDevicesTable;

@end

@implementation DeviceSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    devices = [NSMutableArray new];
    
    BrainBitManager* manager = [BrainBitManager sharedInstance];
    manager.deviceStateDelegate = self;
    
    self.availableDevicesTable.delegate=self;
    self.availableDevicesTable.dataSource = self;
    
    searching = false;
    // Do any additional setup after loading the view.
}

- (IBAction)Search:(id)sender {
    BrainBitManager* manager = [BrainBitManager sharedInstance];
    searching = !searching;
    if(searching){
        [sender setTitle:@"Stop" forState:UIControlStateNormal];
        [manager startSearch:^(NTDeviceInfo* di){
            [devices addObject:di];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.availableDevicesTable reloadData];
            });
        }];
    }
    else
    {
        [sender setTitle:@"Start" forState:UIControlStateNormal];
        [manager stopSearch];
        [devices removeAllObjects];
        [self.availableDevicesTable reloadData];
    }
    
}

- (void) onStateChange:(NTState)state{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController popViewControllerAnimated:YES];
    });
    
}

#pragma mark - Devices Table
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    BrainBitManager* manager = [BrainBitManager sharedInstance];
    [manager connect:[devices objectAtIndex:indexPath.row]];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [devices count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"DeviceInfoCell";

    UITableViewCell *cell = [self.availableDevicesTable dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];

    NTDeviceInfo* devInfo = [devices objectAtIndex:indexPath.row];
    cell.textLabel.text = devInfo.name;
    cell.detailTextLabel.text = [@(devInfo.serialNumber) stringValue];

    return cell;
}


@end
