//
//  ResistanceViewController.m
//  BrainBitObjcExample
//
//  Created by Aseatari on 20.01.2022.
//

#import <Foundation/Foundation.h>
#import "ResistanceViewController.h"

@interface ResistanceViewController ()
@property (weak, nonatomic) IBOutlet UILabel *O1Value;
@property (weak, nonatomic) IBOutlet UILabel *O2Value;
@property (weak, nonatomic) IBOutlet UILabel *T3Value;
@property (weak, nonatomic) IBOutlet UILabel *T4Value;


@end

@implementation ResistanceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
 
}

-(void)viewDidAppear:(BOOL)animated{
    BrainBitManager* manager = [BrainBitManager sharedInstance];
    manager.resistDataDelegate = self;
    [manager initResistance];
    [manager startResist];
}

- (void) onRecieveResist:(NSNumber *)data name:(NSString*)name{
    NSString* value = [NSString stringWithFormat:@"%.2f kOm", ([data doubleValue] / 1e3)];
    dispatch_async(dispatch_get_main_queue(), ^{
        if([name isEqual: @"O1"]){
            self->_O1Value.text = value;
        }
        if([name isEqual: @"O2"]){
            self->_O2Value.text = value;
        }
        if([name isEqual: @"T3"]){
            self->_T3Value.text = value;
        }
        if([name isEqual: @"T4"]){
            self->_T4Value.text = value;
        }
    });
}

- (void)viewDidDisappear:(BOOL)animated{
    BrainBitManager* manager = [BrainBitManager sharedInstance];
    manager.resistDataDelegate = nil;
    [manager destroyResistance];
    [manager stopResist];
}

@end
