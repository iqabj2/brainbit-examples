//
//  DeviceInfoViewController.m
//  BrainBitObjcExample
//
//  Created by Aseatari on 20.01.2022.
//

#import <Foundation/Foundation.h>
#import "DeviceInfoViewController.h"

@interface DeviceInfoViewController ()
@property (weak, nonatomic) IBOutlet UILabel *diviceInfoLabel;

@end

@implementation DeviceInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    BrainBitManager* manager = [BrainBitManager sharedInstance];
    _diviceInfoLabel.text = [manager getInfo];
    
}

@end
