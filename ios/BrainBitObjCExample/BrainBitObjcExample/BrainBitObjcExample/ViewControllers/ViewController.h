//
//  ViewController.h
//  BrainBitObjcExample
//
//  Created by Aseatari on 19.01.2022.
//

#import <UIKit/UIKit.h>
#import "BrainBitManager.h"

@interface ViewController : UIViewController<DeviceState, DevicePower>


@end

