//
//  ViewController.m
//  BrainBitObjcExample
//
//  Created by Aseatari on 19.01.2022.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIBarButtonItem *connectionState;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *devicePower;

@property (weak, nonatomic) IBOutlet UIButton *DeviceInfoButton;
@property (weak, nonatomic) IBOutlet UIButton *ResistButton;
@property (weak, nonatomic) IBOutlet UIButton *EEGButton;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    BrainBitManager* manager = [BrainBitManager sharedInstance];
    manager.deviceStateDelegate = self;
    manager.devicePowerDelegate = self;
}

- (void)viewDidAppear:(BOOL)animated{
    BrainBitManager* manager = [BrainBitManager sharedInstance];
    
    [self onStateChange:[manager connectionState]];
}

- (void) onStateChange:(NTState)state{
    dispatch_async(dispatch_get_main_queue(), ^{
        switch (state) {
            case NTStateDisconnected:
                
                self->_DeviceInfoButton.enabled = false;
                self->_ResistButton.enabled = false;
                self->_EEGButton.enabled = false;
                
                self->_connectionState.title = @"Disconnected";
                self->_devicePower.title = @"0";
                break;
            case NTStateConnected:
                
                self->_DeviceInfoButton.enabled = true;
                self->_ResistButton.enabled = true;
                self->_EEGButton.enabled = true;
                
                self->_connectionState.title = @"Connected";
                break;
        }
    });
}

- (void) onPowerChange:(NSNumber*)power{
    dispatch_async(dispatch_get_main_queue(), ^{
        self->_devicePower.title = [power stringValue];
    });
}


@end
