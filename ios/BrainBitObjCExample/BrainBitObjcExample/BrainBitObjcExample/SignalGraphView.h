//
//  SignalGraphView.h
//  BrainBitObjcExample
//
//  Created by Aseatari on 21.01.2022.
//

#ifndef SignalGraphView_h
#define SignalGraphView_h

#import "CorePlot/CorePlot.h"

@interface SignalGraphView : CPTGraphHostingView<CPTPlotDataSource, CPTPlotSpaceDelegate>
{
}

- (void) initGraph: (NSString*) name;

- (void) dataChanged: (NSString*) channelName newValues:(NSMutableArray*) newValues;


@end

#endif /* SignalGraphView_h */
