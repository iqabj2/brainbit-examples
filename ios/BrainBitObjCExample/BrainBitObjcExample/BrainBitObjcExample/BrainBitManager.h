//
//  BrainBitManager.h
//  BrainBitObjcExample
//
//  Created by Aseatari on 20.01.2022.
//

#ifndef BrainBitManager_h
#define BrainBitManager_h

#import <neurosdk/neurosdk.h>

@protocol DeviceState <NSObject>
- (void) onStateChange: (NTState)state;
@end

@protocol DevicePower <NSObject>
- (void) onPowerChange: (NSNumber*)power;
@end

@protocol EEGData <NSObject>
- (void) onRecieveSignal: (NSArray*)data name:(NSString*)name;
@end

@protocol ResistData <NSObject>
- (void) onRecieveResist: (NSNumber*)data name:(NSString*)name;
@end

@interface BrainBitManager : NSObject
{
    NTDeviceEnumerator* _deviceEnumerator;
    NTDevice* _device;
    
    NTBatteryChannel* _batteryChannel;
    
    NSMutableDictionary<NSString*, NTResistanceChannel*>* _resistanceChannels;
    NSMutableDictionary<NSString*, NTEegChannel*>* _eegChannels;
    NSMutableDictionary<NSString*, NSNumber*> *_eegChannelsOffsets;
}



+ (id) sharedInstance;


@property(nonatomic, strong) id<DeviceState> deviceStateDelegate;
@property(nonatomic, strong) id<DevicePower> devicePowerDelegate;
@property(nonatomic, strong) id<ResistData> resistDataDelegate;
@property(nonatomic, strong) id<EEGData> eegDataDelegate;

- (void) startSearch:(void (^)(NTDeviceInfo *))searchResult;
- (void) stopSearch;

- (void) connect: (NTDeviceInfo*)devInfo;
@property(atomic) NTState connectionState;
- (void) disconnect;

- (NSString*) getInfo;

- (void) startResist;
- (void) stopResist;

- (void) initResistance;
- (void) destroyResistance;

- (void) startSignal;
- (void) stopSignal;

- (void) initEEG;
- (void) destroyEEG;

@end

#endif /* BrainBitManager_h */
