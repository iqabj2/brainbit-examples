//
//  SignalGraphView.m
//  BrainBitObjcExample
//
//  Created by Aseatari on 21.01.2022.
//

#import <Foundation/Foundation.h>
#import "SignalGraphView.h"


@interface SignalGraphView ()
{
    NSMutableArray *plotData;
    NSInteger currentIndex;
    CPTScatterPlot* plot;
}

@end

@implementation SignalGraphView

NSInteger const maxDataPoints = 250 * 5;

- (void) initGraph: (NSString*) name{
    self.allowPinchScaling = FALSE;
    plotData = [NSMutableArray new];
    currentIndex = 0;
    
    //Configure graph
    CPTXYGraph* graph = [[CPTXYGraph alloc] initWithFrame:self.bounds];
    graph.plotAreaFrame.masksToBorder = false;
    self.hostedGraph = graph;
    graph.backgroundColor = [[UIColor whiteColor] CGColor];
    graph.paddingBottom = 10;
    graph.paddingLeft = 40.0;
    graph.paddingTop = 10;
    graph.paddingRight = 10.0;
    
    //Style for graph title
    CPTMutableTextStyle *titleStyle = [CPTMutableTextStyle new];
    titleStyle.color = CPTColor.blackColor;
    titleStyle.fontName = @"HelveticaNeue-Bold";
    titleStyle.fontSize = 20.0;
    titleStyle.textAlignment = CPTTextAlignmentCenter;
    graph.titleTextStyle = titleStyle;
    
    //Set graph title
    graph.title = name;
    graph.titlePlotAreaFrameAnchor = CPTRectAnchorTop;
    graph.titleDisplacement = CGPointMake(0.0, 0.0);
    
    CPTMutableTextStyle* axisTextStyle = [CPTMutableTextStyle new];
    axisTextStyle.color = CPTColor.blackColor;
    axisTextStyle.fontName = @"HelveticaNeue-Bold";
    axisTextStyle.fontSize = 10.0;
    axisTextStyle.textAlignment = CPTTextAlignmentCenter;
    CPTMutableLineStyle* lineStyle = [CPTMutableLineStyle new];
    lineStyle.lineColor = CPTColor.blackColor;
    lineStyle.lineWidth = 5;
    CPTMutableLineStyle* gridLineStyle = [CPTMutableLineStyle new];
    gridLineStyle.lineColor = CPTColor.grayColor;
    gridLineStyle.lineWidth = 0.5;
    
    
    CPTXYAxisSet* axisSet = (CPTXYAxisSet*)graph.axisSet;
    CPTXYAxis* x = axisSet.xAxis;
    if(x){
        x.majorIntervalLength = @250;
        x.minorTicksPerInterval = 250;
        x.labelTextStyle = axisTextStyle;
        x.minorGridLineStyle = gridLineStyle;
        x.axisLineStyle = lineStyle;
        x.axisConstraints = [[CPTConstraints alloc] initWithLowerOffset:0.0];
        x.delegate = self;
    }

    CPTXYAxis* y = axisSet.yAxis;
    if(y){
        y.majorIntervalLength = @5000;
        y.minorTicksPerInterval = 250;
        y.minorGridLineStyle = gridLineStyle;
        y.labelTextStyle = axisTextStyle;
        y.axisLineStyle = lineStyle;
        y.axisConstraints = [[CPTConstraints alloc] initWithLowerOffset:5.0];
        y.delegate = self;
    }
    
    // Set plot space
    
    double xMin = 0.0;
    double xMax = maxDataPoints;
    double yMin = -20000.0;
    double yMax = 20000.0;
    CPTXYPlotSpace* plotSpace = (CPTXYPlotSpace*)graph.defaultPlotSpace;
    if(plotSpace){
        plotSpace.xRange = [[CPTPlotRange alloc] initWithLocation:[NSNumber numberWithDouble:xMin] length:[NSNumber numberWithDouble:xMax]];
        plotSpace.yRange = [[CPTPlotRange alloc] initWithLocation:[NSNumber numberWithDouble:yMin] length:[NSNumber numberWithDouble:(yMax - yMin)]];
    }
    
    CPTMutableLineStyle* plotLineStile = [CPTMutableLineStyle new];
    plotLineStile.lineJoin = kCGLineJoinRound;
    plotLineStile.lineCap = kCGLineCapRound;
    plotLineStile.lineWidth = 2;
    plotLineStile.lineColor = CPTColor.redColor;
    
    plot = [CPTScatterPlot new];
    plot.dataLineStyle = plotLineStile;
    plot.identifier = @"signal-graph";
    plot.dataSource = self;
    plot.delegate = self;
    [graph addPlot:plot];
}

- (void) dataChanged: (NSString*) channelName newValues:(NSMutableArray*) newValues{
   
    CPTGraph* graph = plot.graph;
    CPTXYPlotSpace* plotSpace = (CPTXYPlotSpace*)graph.defaultPlotSpace;
    currentIndex += [newValues count];
    [plotData addObjectsFromArray:newValues];
    NSInteger location;
    if (currentIndex >= maxDataPoints) {
        location = currentIndex - maxDataPoints;
    } else {
        location = 0;
    }
    plotSpace.xRange = [[CPTPlotRange alloc] initWithLocation:[NSNumber numberWithInteger:location] length:[NSNumber numberWithInteger:maxDataPoints]];
    
    [plot reloadData];
    
}

- (NSUInteger)numberOfRecordsForPlot:(nonnull CPTPlot *)plot{
    return [plotData count];
}

-(nullable NSArray *)numbersForPlot:(nonnull CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndexRange:(NSRange)indexRange{
    if(fieldEnum == CPTScatterPlotFieldX){
        //NSString* time = [NSString stringWithFormat:@"%lu:%02lu", ((indexRange.location + indexRange.length) / 250), ((indexRange.location + indexRange.length) % 250 * 60 / 250 )];
        //NSLog(@"%@", time);
        NSMutableArray *array = [NSMutableArray array];
        for (NSUInteger i = indexRange.location; i < indexRange.location + indexRange.length; i++) {
            [array addObject:[NSNumber numberWithUnsignedInteger:i]];
        }
        return array;
    }else if(fieldEnum == CPTScatterPlotFieldY){
        return [plotData objectsAtIndexes:[[NSIndexSet alloc] initWithIndexesInRange:indexRange]];
    }
    return 0;
}



@end
